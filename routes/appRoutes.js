
var pageblogindex=require('../lib/pageblogindex') ;
var pageLogin=require('../lib/pageLogin') ;
var pageIndex=require('../lib/pageIndex') ;
var pageBlog=require('../lib/pageBlog') ;
var pageCaipu=require('../lib/pageCaipu.js') ;
var pageboke=require('../lib/pageboke') ;
var pageweixin=require('../lib/pageweixin') ;
var pageCt=require('../lib/pageCt') ;
var pageCanting=require('../lib/pageCanting');
var pageRandom=require('../lib/pageRandom');
var pagecaiming=require('../lib/pagecaiming') ;
var pagecategory=require('../lib/pagecategory') ;
var pageRss=require('../lib/pageRss');
var pageDingcan=require('../lib/pageDingcan');
var pageIp=require('../lib/pageIp');
module.exports = function (app) {
    //login
    app.get('/',pageblogindex.index) ;
    app.get('/root.txt',pageblogindex.baidu) ;
    app.get('/ip',pageIp.index) ;
    app.get('/page_:page.html',pageblogindex.index) ;
    app.get('/category/:id.html',pagecategory.index) ;
    app.get('/category/:id/:page.html',pagecategory.index) ;
    app.get('/rss',pageRss.index) ;
    app.all('/weixin/:id/',pageweixin.index) ;
    app.all('/ct/denglu/',pageCt.index) ;
    app.get('/ct/caim',pagecaiming.index) ;

    app.get('/dingcan',pageDingcan.index) ;


    app.get('/ct/random',pageRandom.index);

    app.get('/food',pageCaipu.index) ;
    app.get('/boke',pageboke.index) ;
    app.get('/blog/:id.html',pageboke.blog1) ;
    app.get('/admin',pageLogin.index) ;
    app.all('/Login/',pageLogin.login) ;
    app.all('/ct/diningRoom',pageCanting.index);
    app.all('*',pageLogin.checkLogin) ;
    app.all('/index/',pageIndex.index) ;
    app.all('/admin/blog/',pageBlog.index) ;


}