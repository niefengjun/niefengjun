/**
 * Created by wangdonghui on 14-1-7.
 */
var _PageSize = 15;

$(function () {
    $('#sltPage').change(function () {
        ajaxGetList(true);
    });
    $('#btnSubmit').click(function () {
        _CurrentPage = 1;
        ajaxGetList(true);
    });
    ajaxGetList(true);
})

function ajaxGetList(GetPageInfo) {
    _PageSize = $('#sltPage').val();
    $('#ajaxDataList').hide();
    $('#ajaxDataLoading').show();
    $.ajax({
        type: "GET",
        url: "../devices/",
        data: "AjaxType=getPageData&pageSize=" + _PageSize + '&currentPage=' + _CurrentPage
            + "&time=" + new Date(),
        success: function (result) {
            // alert(result['pageInfo']);
            if (GetPageInfo) {
                pageInfoDisplay(result['pageInfo']);
            }
            // alert(result);
            var html = "";
            if (result.pageData.length > 0) {
                var items = result['pageData'];
                for (var i = 0; i < items.length; i++) {
                    // <td><mark><span class="highlight">无</span></mark></td>
                    // alert(items[i]);
                    html += '<tr>';
                    html += '<td class="auto">' + items[i].sn + '</td>';
                    html += '<td class="auto">' + items[i].mac + '</td>';
                    html += '<td>' + convertTimestampToFullDate(items[i].created) + '</td>';
                    html += '<td>' + items[i].createdBy + '</td>';
                    html += '</tr>';
                }
            } else {
                html = "<tr><td colspan='10'>未找到任何数据</td></tr>";
            }
            $('#ajaxDataList').html(html);
            $('#ajaxDataList').show();
            $('#ajaxDataLoading').hide();
        }
    });
}