var _PageSize=15;

$(function(){
    $('#btnSubmit').click(function () {_CurrentPage=1;ajaxGetList(true); });
	ajaxGetList(true);
})

function ajaxGetList(GetPageInfo) {
	_PageSize=$('#sltPage').val();
	$('#ajaxDataList').hide();
	$('#ajaxDataLoading').show();
	$.ajax({
	    type: "GET", 
	    url: "../channel/list", 
	    data: "AjaxType=pagePageInfo&pageSize="+_PageSize+'&currentPage='+_CurrentPage
	        +"&time=" + new Date(), 
	    success: function (result) {
	    	// alert(result['pageInfo']);
	    	if(GetPageInfo)
	    	{
	    		pageInfoDisplay(result['pageInfo']);
	    	}
		    // alert(result);
		     var html="";
			 if(result.pageData.length>0) 
			 {
			  	var items=result['pageData'];
			  	for(var i=0;i<items.length;i++)
			  	{
					// <td><mark><span class="highlight">无</span></mark></td>
					// alert(items[i]);
			  	 	html+='<tr>';
			  	 	html+='<td class="auto">';
			  	 	html+='<a href="#" onclick="showMediumModal(\'/appInfo/'+items[i].id +'\',600)">'+items[i].nickName +'</a></td>';
			    	html+='<td>'+items[i].email+'</td>';
			    	html+='<td>'+items[i].contacts+'</td>';
			    	if(items[i].status=='verify')
			    	{
			    		html+='<td>未提交</td>';
			    	}else if(items[i].status=='verified')
			    	{
			    		html+='<td>已通过</td>';
			    	}else if(items[i].status=='pass')
			    	{
			    		html+='<td>未通过</td>';
			    	}else  if(items[i].status=='waitVerify')
			    	{
			    		html+='<td>待审核</td>';
			    	}else  if(items[i].status=='blocked')
			    	{
			    		html+='<td>锁定</td>';
			    	}
			    	html+='<td>查看</td>';
			    	html+='</tr>';

			  }
			}else
			{
	                html="<tr><td colspan='10'>未找到任何数据</td></tr>";
			}
			$('#ajaxDataList').html(html);
			$('#ajaxDataList').show();
			$('#ajaxDataLoading').hide();
		}
	});
}