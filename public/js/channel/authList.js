var _PageSize=15;

$(function(){
    $('#btnSubmit').click(function () {_CurrentPage=1;ajaxGetList(true); });
	ajaxGetList(true);
})

function ajaxGetList(GetPageInfo) {
	_PageSize=$('#sltPage').val();
	$('#ajaxDataList').hide();
	$('#ajaxDataLoading').show();
	$.ajax({
	    type: "GET", 
	    url: "../channel/authList", 
	    data: "AjaxType=pagePageInfo&pageSize="+_PageSize+'&currentPage='+_CurrentPage
	        +"&time=" + new Date(), 
	    success: function (result) {
	    	// alert(result['pageInfo']);
	    	if(GetPageInfo)
	    	{
	    		pageInfoDisplay(result['pageInfo']);
	    	}
		    // alert(result);
		     var html="";
			 if(result.pageData.length>0) 
			 {
			  	var items=result['pageData'];
			  	for(var i=0;i<items.length;i++)
			  	{
			  	 	html+='<tr>';
			    	html+='<td>'+items[i].channel.channelID+'</td>';
			  	 	html+='<td class="auto">';
			  	 	html+='<a href="#" onclick="showModal(\''+items[i].channel.user +'\')">'+items[i].user.nickName +'</a></td>';
			    	html+='<td>'+items[i].channel.company+'</td>';
			    	html+='<td>'+items[i].user.email+'</td>';
			    	html+='<td>'+items[i].channel.mobile+'</td>';
			    	if(items[i].user.status=='waitVerify')
			    	{
			    		html+='<td>审核中</td>';
			    	}else if(items[i].user.status=='pass')
			    	{
			    		html+='<td>已通过</td>';
			    	}else
			    	{
			    		html+='<td>未通过</td>';
			    	}
			    	html+='<td><a href="#" onclick="showModal(\''+items[i].channel.user +'\')">审核</a></td>';
			    	html+='</tr>';

			  }
			}else
			{
	                html="<tr><td colspan='6'>未找到任何数据</td></tr>";
			}
			$('#ajaxDataList').html(html);
			$('#ajaxDataList').show();
			$('#ajaxDataLoading').hide();
		}
	});
}

function showModal(uuid){
	 $.fancybox({
        'autoScale': true,
        'showNavArrows': false,
        'transitionIn': 'none',
        'transitionOut': 'none',
        'scrolling':false,
        'hideOnOverlayClick':false,
        'type': 'iframe',
        'width':750,
        'height':550,
        'href': '../channel/auth/'+uuid
        
    });
}