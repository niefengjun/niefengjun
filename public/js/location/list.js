var _PageSize=15;

$(function(){
    $('#btnSubmit').click(function () {_CurrentPage=1;ajaxGetList(true); });
	ajaxGetList(true);
})

function ajaxGetList(GetPageInfo) {
	_PageSize=$('#sltPage').val();
	$('#ajaxDataList').hide();
	$('#ajaxDataLoading').show();
	$.ajax({
	    type: "GET", 
	    url: "../channel/list", 
	    data: "AjaxType=pagePageInfo&pageSize="+_PageSize+'&currentPage='+_CurrentPage
	        +"&time=" + new Date(), 
	    success: function (result) {
	    	// alert(result['pageInfo']);
	    	if(GetPageInfo)
	    	{
	    		pageInfoDisplay(result['pageInfo']);
	    	}
		    // alert(result);
		     var html="";
			 if(result.pageData.length>0) 
			 {
			  	var items=result['pageData'];
			  	for(var i=0;i<items.length;i++)
			  	{
					// <td><mark><span class="highlight">无</span></mark></td>

			  	 	html+='<tr>';
			  	 	html+='<td class="auto">';
			  	 	html+='<a href="#" onclick="showMediumModal(\'/appInfo/'+items[i].id +'\',600)">'+items[i].nickName +'</a></td>';
			    	html+='<td>'+items[i].email+'</td>';
			    	html+='<td>'+items[i].contacts+'</td>';
			    	if(items[i].verify=='verify')
			    	{
			    		html+='<td>'+items[i].审核中+'</td>';
			    	}else if(items[i].verify=='pass')
			    	{
			    		html+='<td>'+items[i].已通过+'</td>';
			    	}else
			    	{
			    		html+='<td>'+items[i].未通过+'</td>';
			    	}
			    	html+='<td>'+items[i].developer +'</td>';
			    	html+='<td>操作</td>';
			    	html+='</tr>';

			  }
			}else
			{
	                html="<tr><td colspan='10'>未找到任何数据</td></tr>";
			}
			$('#ajaxDataList').html(html);
			$('#ajaxDataList').show();
			$('#ajaxDataLoading').hide();
		}
	});
}