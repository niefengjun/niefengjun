/**
 * Created by wangdonghui on 14-3-3.
 */
var _PageSize = 15;
var _pageInfo = true;

$(function () {
    pageInit();
    $('#btnSubmit').click(function () {
        _CurrentPage = 1;
        ajaxGetList();
    });
    // ajaxGetList();
    fixedLayer('inner');
})

function pageInit() {
    var startDate = new Date().getFullYear() + '-'
        + ('0' + (new Date().getMonth() + 1)).slice(-2) + '-'
        + '01';
    var endDate = new Date().getFullYear() + '-'
        + ('0' + (new Date().getMonth() + 1)).slice(-2) + '-'
        + ('0' + (new Date().getDate())).slice(-2);
    $('#txtStartDate').val(startDate);
    $('#txtEndDate').val(endDate);
}

function ajaxGetList() {
    _PageSize = $('#sltPage').val();
    $('#ajaxDataList').hide();
    $('#ajaxDataLoading').show();
    $.ajax({
        type: "GET",
        url: "../dataCount/deviceWareHouseDailyReportList",
        data: "AjaxType=getPageData&pageSize=" + _PageSize + '&currentPage=' + _CurrentPage + "&timeBegin=" + $('#txtStartDate').val() + "&timeEnd=" + $('#txtEndDate').val() + "&time=" + new Date(),
        success: function (result) {
            // alert(result['pageInfo']);
            if (_pageInfo) {
                pageInfoDisplay(result['pageInfo']);
            }
            // alert(result);
            var html = "";
            if (result.pageData.length > 0) {
                var items = result['pageData'];
                for (var i = 0; i < items.length; i++) {

                    html += '<tr>';
                    html += '<td>' + items[i].reportDay + '</td>';
                    html += '<td>' + items[i].deviceCount + '</td>';
                    html += '<td>' + items[i].deviceInCount + '</td>';
                    html += '<td>' + items[i].deviceOutCount + '</td>';
                    html += '<td>' + items[i].deviceUsedCount + '</td>';
                    html += '<td>' + items[i].deviceInStatusCount + '</td>';
                    html += '<td>' + items[i].deviceTestCount + '</td>';
                    html += '<td>' + items[i].deviceDepositCount + '</td>';
                    html += '<td>' + items[i].deviceSellCount + '</td>';
                    html += '<td>' + items[i].deviceInternalFrictionCount + '</td>';
                    html += '<td>' + items[i].deviceOnlineCount + '</td>';
                    html += '</tr>';

                }
            } else {
                html = "<tr><td colspan='11'>未找到任何数据</td></tr>";
            }
            $('#ajaxDataList').html(html);
            $('#ajaxDataList').show();
            $('#ajaxDataLoading').hide();
        }
    });
}
