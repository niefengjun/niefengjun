$(function(){
	// pageInit();
    $('#btnSubmit').click(function () {_CurrentPage=1;ajaxGetList(); });
	ajaxGetList();
	fixedLayer('inner');
})

function ajaxGetList() {
	_PageSize=$('#sltPage').val();
	$('#ajaxDataList').hide();
	$('#ajaxDataLoading').show();
	$.ajax({
	    type: "GET", 
	    url: "../dataCount/shopOnlineReportDeatail", 
	    data: "AjaxType=pagePageInfo"
	    	+"&uuid="+$('#uuid').val()
	    	+"&timeBegin="+$('#txtStartDate').val()
	    	+"&timeEnd="+$('#txtEndDate').val()
	    	+"&locationID="+$('#txtID').val()
	        +"&time=" + new Date(), 
	    success: function (result) {

		     var dayCount=1;
		     if(result["dayCount"])
		     {
		     	dayCount=parseInt(result["dayCount"]);
		     }
		     var html="";
			 if(result.pageData.length>0) 
			 {
			  if(result['summaryInfo'])
			  {
			  	html+='<tr><td class="auto">总计(平均)：</td>'
					+'<td width="120">'+result['summaryInfo'].devicesCount+'</td>'
					+'<td width="120"></td>'
					+'<td width="120">'+result['summaryInfo'].visitorCount+' ('+((result['summaryInfo'].visitorCount/dayCount).toFixed(2))+')</td>'
					+'<td width="120">'+result['summaryInfo'].uvCount+' ('+((result['summaryInfo'].uvCount/dayCount).toFixed(2))+')</td>'
					+'<td width="120">'+result['summaryInfo'].avgCount+'</td>'
				}

			  	var items=result['pageData'];
			  	for(var i=0;i<items.length;i++)
			  	{
					// <td><mark><span class="highlight">无</span></mark></td>

			  	 	html+='<tr>';
			    	html+='<td class="auto">'+items[i].day+'</td>';
			    	html+='<td width="120">'+items[i].devicesCount+'</td>';
			    	html+='<td width="120">'+items[i].hightCount+'</td>';
			    	html+='<td width="120">'+items[i].visitorCount+'</td>';
			    	html+='<td width="120">'+items[i].uvCount+'</td>';
			    	html+='<td width="120">'+items[i].avgCount+'</td>';
			    	html+='</tr>';

			  }
			}else
			{
	                html="<tr><td colspan='6'>未找到任何数据</td></tr>";
			}
			$('#ajaxDataList').html(html);
			$('#ajaxDataList').show();
			$('#ajaxDataLoading').hide();
		}
	});
}
