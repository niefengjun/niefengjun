var _PageSize=15;
var _pageInfo=true;

$(function(){
	pageInit();
    $('#btnSubmit').click(function () {_CurrentPage=1;ajaxGetList(); });
	// ajaxGetList();
	fixedLayer('inner');
})

function pageInit()
{
	$('#area').citySelect({
		url:'/city/',
	    prov:"",
	    city:"",
	    provPre:{text:"全部省份",value:"-1"},
	    cityPre:{text:"全部城市",value:"-1"}
	});

    var startDate=new Date().getFullYear()+ '-'
        + ('0' + (new Date().getMonth() + 1)).slice(-2)+ '-'
        + ('0' + (new Date().getDate())).slice(-2);

    $('#txtStartDate').val(startDate);
}

function ajaxGetList() {
	_PageSize=$('#sltPage').val();
	$('#ajaxDataList').hide();
	$('#ajaxDataLoading').show();
	$.ajax({
	    type: "GET",
	    url: "../dataCount/channelOnlineReport", 
	    data: "AjaxType=pagePageInfo&pageSize="+_PageSize+'&currentPage='+_CurrentPage
	    	+"&id="+$('#txtID').val()
	    	+"&timeBegin="+$('#txtStartDate').val()
	    	+"&province="+($('#sltProvince').val()==null?"-1":$('#sltProvince').val())
	    	+"&city="+($('#sltCity').val()==null?"-1":$('#sltCity').val())
	        +"&time=" + new Date(), 
	    success: function (result) {
	    	// alert(result['pageInfo']);
	    	if(_pageInfo)
	    	{
	    		pageInfoDisplay(result['pageInfo']);
	    	}
		    // alert(result);
		     var html="";
			 if(result.pageData.length>0) 
			 {
			  if(result['summaryInfo'])
			  {
			  	html+='<tr><td colspan="3">总计：</td>'
					+'<td width="80">'+result['summaryInfo'].devicesCount+'</td>'
					+'<td width="80">'+result['summaryInfo'].devicesUseCount+'</td>'
					+'<td width="80"></td>'
					+'<td width="80">'+result['summaryInfo'].visitorCount+'</td>'
					+'<td width="80">'+result['summaryInfo'].uvCount+'</td>'
					+'<td width="80"></td>'
					
				}

			  	var items=result['pageData'];
			  	for(var i=0;i<items.length;i++)
			  	{
					// <td><mark><span class="highlight">无</span></mark></td>

			  	 	html+='<tr>';
			    	html+='<td width="80">'+items[i].channelID+'</td>';
			    	html+='<td class="auto">'+items[i].company+'</td>';
			    	html+='<td width="80">'+(items[i].area?items[i].area:'')+'</td>';
			    	html+='<td width="80">'+items[i].devicesCount+'</td>';
			    	html+='<td width="80">'+items[i].devicesUseCount+'</td>';
			    	html+='<td width="80">'+items[i].hightCount+'</td>';
			    	html+='<td width="80">'+items[i].visitorCount+'</td>';
			    	html+='<td width="80">'+items[i].uvCount+'</td>';
			    	html+='<td width="80"><a href="#" onclick="showModal(\''+items[i].user +'\')">查看详细</a></td>';
			    	
			    	html+='</tr>';

			  }
			}else
			{
	                html="<tr><td colspan='8'>未找到任何数据</td></tr>";
			}
			$('#ajaxDataList').html(html);
			$('#ajaxDataList').show();
			$('#ajaxDataLoading').hide();
		}
	});
}

function showModal(uuid){
	 window.location='/dataCount/channelOnlineReportDeatail/'+uuid+'/'+$('#txtStartDate').val();
}