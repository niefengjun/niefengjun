var _PageSize=15;
var _pageInfo=true;

$(function(){
	pageInit();
    $('#btnSubmit').click(function () {_CurrentPage=1;ajaxGetList(); });
	// ajaxGetList();
	fixedLayer('inner');
})

function pageInit()
{
	$('#area').citySelect({
		url:'/city/',
	    prov:"",
	    city:"",
	    provPre:{text:"全部省份",value:"-1"},
	    cityPre:{text:"全部城市",value:"-1"}
	});

	var txtBegin=new Date().getFullYear()+ '-'
        + ('0' + (new Date().getMonth() + 1)).slice(-2)+ '-'
        + ('0' + new Date().getDate()).slice(-2);
    $('#txtTimeBegin').val(txtBegin);
}
function ajaxGetList() {
	_PageSize=$('#sltPage').val();
	$('#ajaxDataList').hide();
	$('#ajaxDataLoading').show();
	$.ajax({
	    type: "GET", 
	    url: "../dataCount/shopOnlineDayReport", 
	    data: "AjaxType=pagePageInfo&pageSize="+_PageSize+'&currentPage='+_CurrentPage
	    	+"&id="+$('#txtID').val()
	    	+"&service="+encodeURI($('#stlService').val())
	    	+"&timeBegin="+$('#txtTimeBegin').val()
	    	+"&province="+($('#sltProvince').val()==null?"-1":$('#sltProvince').val())
	    	+"&city="+($('#sltCity').val()==null?"-1":$('#sltCity').val())
	        +"&time=" + new Date(), 
	    success: function (result) {
	    	// alert(result['pageInfo']);
	    	if(_pageInfo)
	    	{
	    		pageInfoDisplay(result['pageInfo']);
	    	}
		    // alert(result);
		     var html="";
			 if(result.pageData.length>0) 
			 {
			  if(result['summaryInfo'])
			  {
			  	html+='<tr><td colspan="5">总计：</td>'
					+'<td>'+result['summaryInfo'].devicesCount+'</td>'
					+'<td>'+result['summaryInfo'].hightCount+'</td>'
					+'<td>'+result['summaryInfo'].visitorCount+'</td>'
					+'<td>'+result['summaryInfo'].uvCount+'</td>'
					+'<td>'+result['summaryInfo'].currentCount+'</td>'
					+'<td>'+result['summaryInfo'].currentUVCount+'</td>'
					+'<td>'+result['summaryInfo'].appCount+'</td>'
					+'<td>'+(result['summaryInfo'].uvCount==0?'0':((result['summaryInfo'].appCount/result['summaryInfo'].uvCount).toFixed(2)))+'</td></tr>';
			  }

			  	var items=result['pageData'];
			  	for(var i=0;i<items.length;i++)
			  	{
					// <td><mark><span class="highlight">无</span></mark></td>

			  	 	html+='<tr>';
			    	html+='<td width="70">'+(items[i].ids?items[i].ids[0]:'')+'</td>';
			    	html+='<td class="90">'+items[i].owner+'</td>';
			  	 	html+='<td class="auto">'+items[i].title +'</td>';
			    	html+='<td width="65">'+items[i].area+'</td>';
			    	html+='<td width="60">'+(items[i].services?items[i].services[0]:'')+'</td>';
			    	html+='<td width="65">'+items[i].devicesCount+'</td>';
			    	html+='<td width="65">'+items[i].hightCount+'</td>';
			    	html+='<td width="65">'+items[i].visitorCount+'</td>';
			    	html+='<td width="65">'+items[i].uvCount+'</td>';
			    	html+='<td width="65">'+items[i].currentCount+'</td>';
			    	html+='<td width="65">'+items[i].currentUVCount+'</td>';
			    	html+='<td width="70">'+items[i].appCount+'</td>';
			    	html+='<td width="65">'+(items[i].uvCount==0?'0':((items[i].appCount/items[i].uvCount).toFixed(2)))+'</td>';
			    	html+='</tr>';

			  }
			}else
			{
	                html="<tr><td colspan='14'>未找到任何数据</td></tr>";
			}
			$('#ajaxDataList').html(html);
			$('#ajaxDataList').show();
			$('#ajaxDataLoading').hide();
		}
	});
}
// function showModal(uuid){
// 	 $.fancybox({
//         'autoResize': true,
//         'type': 'iframe',
//         'width':750,
//         'height':550,
// 		'afterClose':ajaxGetList,
//         'href': '/location/shopInfo/'+uuid        
//     });
// }