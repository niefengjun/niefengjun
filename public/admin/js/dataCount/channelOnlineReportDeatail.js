
$(function(){
	// pageInit();
    $('#btnSubmit').click(function () {ajaxGetList(); });
	ajaxGetList();
	fixedLayer('inner');
})


function ajaxGetList() {
	$('#ajaxDataList').hide();
	$('#ajaxDataLoading').show();
	$.ajax({
	    type: "GET", 
	    url: "../dataCount/channelOnlineReportDeatail", 
	    data: "AjaxType=pagePageInfo"
	    	+"&uuid="+ $('#uuid').val()
	    	+"&channelID="+ $('#txtID').val()
	    	+"&company="+ encodeURI($('#txtCompany').val())
	    	+"&timeBegin="+$('#txtStartDate').val()
	    	+"&timeEnd="+$('#txtEndDate').val()
	        +"&time=" + new Date(), 
	    success: function (result) {

		     var dayCount=1;
		     if(result["dayCount"])
		     {
		     	dayCount=parseInt(result["dayCount"]);
		     }
		     var html="";
			 if(result.pageData.length>0) 
			 {
			  if(result['summaryInfo'])
			  {
			  	html+='<tr><td class="auto">总计(平均)：</td>'
					+'<td width="120"></td>'
					+'<td width="120"></td>'
					+'<td width="120">'+result['summaryInfo'].visitorCount+' ('+((result['summaryInfo'].visitorCount/dayCount).toFixed(2))+')</td>'
					+'<td width="120">'+result['summaryInfo'].uvCount+' ('+((result['summaryInfo'].uvCount/dayCount).toFixed(2))+')</td>'
				}

			  	var items=result['pageData'];
			  	for(var i=0;i<items.length;i++)
			  	{
					// <td><mark><span class="highlight">无</span></mark></td>
			  	 	html+='<tr>';
			    	html+='<td class="auto">'+items[i].day+'</td>';
			    	html+='<td width="120">'+items[i].devicesUseCount+'</td>';
			    	html+='<td width="120">'+items[i].hightCount+'</td>';
			    	html+='<td width="120">'+items[i].visitorCount+'</td>';
			    	html+='<td width="120">'+items[i].uvCount+'</td>';
			    	html+='</tr>';
			    }
			}else
			{
	                html="<tr><td colspan='5'>未找到任何数据</td></tr>";
			}
			$('#ajaxDataList').html(html);
			$('#ajaxDataList').show();
			$('#ajaxDataLoading').hide();

			//渠道信息
			if(result['channel'])
			{
				$('#company').html(result['channel'].channel.company+'('+result['channel'].channel.channelID+')');
				$('#area').html(result['channel'].channel.area);
				$('#shopCount').html(result['channel'].shopCount);
				$('#deviceCount').html(result['channel'].deviceCount);
				$('#deviceUseCount').html(result['channel'].devicesUseCount);
			}else
			{
				$('#company').html('');
				$('#area').html('');
				$('#shopCount').html('');
				$('#deviceCount').html('');
				$('#deviceUseCount').html('');
			}
		}
	});
}
