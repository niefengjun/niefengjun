var _PageSize=15;

$(function(){
	pageInit();
    $('#btnSearch').click(function () {_CurrentPage=1;ajaxGetList(true); });

    //全选
	$("#checkAll").click(function () {
		if ($(this).is(":checked")) {
			$("input[type='checkbox']").attr("checked", "checked");
		}
		else {
			$("input[type='checkbox']").removeAttr("checked");
		}
	});
	// ajaxGetList(true);
	fixedLayer('inner');
})

function pageInit()
{
	// $('#area').citySelect({
	// 	url:'../js/city.js',
	//     prov:"",
	//     city:"",
	//     provPre:{text:"全部省份",value:"-1"},
	//     cityPre:{text:"全部城市",value:"-1"}
	// });

	var startDate=new Date().getFullYear()+ '-'
        + ('0' + (new Date().getMonth() + 1)).slice(-2)+ '-'
        + '01';
    var endDate=new Date().getFullYear()+ '-'
        + ('0' + (new Date().getMonth() + 1)).slice(-2)+ '-'
        + ('0' + (new Date().getDate())).slice(-2);
    $('#txtStartDate').val(startDate);
    $('#txtEndDate').val(endDate);
}

function ajaxGetList(GetPageInfo) {
	_PageSize=$('#sltPage').val();
	$('#ajaxDataList').hide();
	$('#ajaxDataLoading').show();

	$.ajax({
	    type: "GET", 
	    url: "../dataCount/portalPVReport", 
	    data: "AjaxType=pagePageInfo&pageSize="+_PageSize+'&currentPage='+_CurrentPage
	    	// +"&from="+$('#sltFrom').val()
	    	// +"&appid="+$('#txtAppid').val()
	    	// +"&className="+($('#txtClass').val().length>0?encodeURI($('#txtClass').val()):'')
	    	// +"&role="+$('#sltRole').val()
	    	// +"&owner="+$('#txtOwner').val()
	    	+"&timeBegin="+($('#txtStartDate').val().length>0?($('#txtStartDate').val()+' 00:00:00'):"")
	    	+"&timeEnd="+($('#txtEndDate').val().length>0?($('#txtEndDate').val()+' 23:29:29'):"")
	        +"&time=" + new Date(), 
	    success: function (result) {
	    	// alert(result['pageInfo']);
	    	// if(GetPageInfo)
	    	// {
	    	// 	pageInfoDisplay(result['pageInfo']);
	    	// }
		    // alert(result);
		     var html="";
		     if(result.summaryInfo)
		     {     	
		  	 	html+='<tr>';
		    	html+='<td>总计：</td>';
		    	html+='<td>'+result.summaryInfo.appcenter+'</td>';	    
		    	html+='<td>'+result.summaryInfo.m5334+'</td>'
		    	html+='<td>'+result.summaryInfo.baidusearch+'</td>'
		    	html+='<td>'+result.summaryInfo.entry+'/'+result.summaryInfo.pv+'</td>'
		    	html+='<td>'+result.summaryInfo.incConnect+'</td>'
		    	html+='<td>'+result.summaryInfo.portal+'/'+result.summaryInfo.portalpv+'</td>'
		    	html+='<td>'+result.summaryInfo.incClick+'</td>'
		    	html+='<td>'+result.summaryInfo.summary+'</td>'
		    	html+='</tr>';
		     }
			 if(result.pageData&&result.pageData.length>0) 
			 {
			  	var items=result['pageData'];
			  	for(var i=0;i<items.length;i++)
			  	{
			  	 	html+='<tr>';
			    	html+='<td class="auto">'+items[i].day+'</td>';	    
			    	html+='<td width="110">'+items[i].appcenter+'</td>';	    
			    	html+='<td width="110">'+items[i].m5334+'</td>'
			    	html+='<td width="110">'+items[i].baidusearch+'</td>'
			    	html+='<td width="110">'+items[i].entry+'/'+items[i].pv+'</td>'
			    	html+='<td width="110">'+items[i].incConnect+'</td>'
			    	html+='<td width="110">'+items[i].portal+'/'+items[i].portalpv+'</td>'
			    	html+='<td width="110">'+items[i].incClick+'</td>'
			    	html+='<td width="110">'+items[i].summary+'</td>'
			    	html+='</tr>';
			  }
			}else
			{
	                html="<tr><td colspan='8'>未找到任何数据</td></tr>";
			}
			$('#ajaxDataList').html(html);
			$('#ajaxDataList').show();
			$('#ajaxDataLoading').hide();
		}
	});
}
