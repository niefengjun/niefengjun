var _PageSize=15;
var _pageInfo=true;

$(function(){
	pageInit();
    $('#btnSubmit').click(function () {_CurrentPage=1;ajaxGetList(); });
	// ajaxGetList();
	fixedLayer('inner');
})

function pageInit()
{
	$('#area').citySelect({
		url:'/city/',
	    prov:"",
	    city:"",
	    provPre:{text:"全部省份",value:"-1"},
	    cityPre:{text:"全部城市",value:"-1"}
	});

	var startDate=new Date().getFullYear()+ '-'
        + ('0' + (new Date().getMonth() + 1)).slice(-2)+ '-'
        + '01';
    var endDate=new Date().getFullYear()+ '-'
        + ('0' + (new Date().getMonth() + 1)).slice(-2)+ '-'
        + ('0' + (new Date().getDate())).slice(-2);
    $('#txtStartDate').val(startDate);
    $('#txtEndDate').val(endDate);
}

function ajaxGetList() {
	_PageSize=$('#sltPage').val();
	$('#ajaxDataList').hide();
	$('#ajaxDataLoading').show();
	$.ajax({
	    type: "GET", 
	    url: "../dataCount/shopOnlineReport", 
	    data: "AjaxType=pagePageInfo&pageSize="+_PageSize+'&currentPage='+_CurrentPage
	    	+"&id="+$('#txtID').val()
	    	+"&service="+encodeURI($('#stlService').val())
	    	+"&timeBegin="+$('#txtStartDate').val()
	    	+"&timeEnd="+$('#txtEndDate').val()
	    	+"&province="+($('#sltProvince').val()==null?"-1":$('#sltProvince').val())
	    	+"&city="+($('#sltCity').val()==null?"-1":$('#sltCity').val())
	        +"&time=" + new Date(), 
	    success: function (result) {
	    	// alert(result['pageInfo']);
	    	if(_pageInfo)
	    	{
	    		pageInfoDisplay(result['pageInfo']);
	    	}
		    // alert(result);
		     var html="";
			 if(result.pageData.length>0) 
			 {
			  if(result['summaryInfo'])
			  {
			  	html+='<tr><td colspan="5">总计：</td>'
					+'<td width="60">'+result['summaryInfo'].devicesCount+'</td>'
					+'<td width="65">'+result['summaryInfo'].visitorCount+'</td>'
					+'<td width="85">'+result['summaryInfo'].uvCount+'</td>'
					+'<td width="65">'+result['summaryInfo'].avgCount+'</td>'
					+'<td width="65">'+result['summaryInfo'].appCount+'</td>'
					+'<td width="65">'+(result['summaryInfo'].uvCount==0?'0':((result['summaryInfo'].appCount/result['summaryInfo'].uvCount).toFixed(2)))+'</td>'
					+'<td width="65">'+''+'</td></tr>';
			  }

			  	var items=result['pageData'];
			  	for(var i=0;i<items.length;i++)
			  	{
			  	 	html+='<tr>';
			    	html+='<td width="70">'+(items[i].ids?items[i].ids[0]:'')+'</td>';
			    	html+='<td class="auto">'+items[i].title+'</td>';
			    	html+='<td width="120">'+(items[i].channelName?items[i].channelName:"")+'</td>';
			    	html+='<td width="60">'+items[i].area+'</td>';
			    	html+='<td width="60">'+items[i].services+'</td>';
			    	html+='<td width="60">'+items[i].devicesCount+'</td>';
			    	html+='<td width="65">'+items[i].visitorCount+'</td>';
			    	html+='<td width="85">'+items[i].uvCount+'</td>';
					html+='<td width="65">'+items[i].avgCount+'</td>';
			    	html+='<td width="65">'+items[i].appCount+'</td>';
			    	html+='<td width="65">'+(items[i].uvCount==0?'0':((items[i].appCount/items[i].uvCount).toFixed(2)))+'</td>';
			    	html+='<td width="50"><a href="#" onclick="showModal(\''+items[i]._id +'\')">查看详细</a></td>';
			    	html+='</tr>';
			    }
			}else
			{
	                html="<tr><td colspan='13'>未找到任何数据</td></tr>";
			}
			$('#ajaxDataList').html(html);
			$('#ajaxDataList').show();
			$('#ajaxDataLoading').hide();
		}
	});
}

function showModal(uuid){
	 window.location='/dataCount/shopOnlineReportDeatail/'+uuid+'/'+$('#txtStartDate').val()+'/'+$('#txtEndDate').val();
}