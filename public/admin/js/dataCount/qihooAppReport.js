var _PageSize=15;

$(function(){
	pageInit();
    $('#btnSearch').click(function () {_CurrentPage=1;ajaxGetList(true); });

    //全选
	$("#checkAll").click(function () {
		if ($(this).is(":checked")) {
			$("input[type='checkbox']").attr("checked", "checked");
		}
		else {
			$("input[type='checkbox']").removeAttr("checked");
		}
	});
	// ajaxGetList(true);
	fixedLayer('inner');
})

function pageInit()
{
	$('#area').citySelect({
		url:'/city/',
	    prov:"",
	    city:"",
	    provPre:{text:"全部省份",value:"-1"},
	    cityPre:{text:"全部城市",value:"-1"}
	});
	var startDate=new Date().getFullYear()+ '-'
        + ('0' + (new Date().getMonth() + 1)).slice(-2)+ '-'
        + '01';
    var endDate=new Date().getFullYear()+ '-'
        + ('0' + (new Date().getMonth() + 1)).slice(-2)+ '-'
        + ('0' + (new Date().getDate())).slice(-2);
    $('#txtStartDate').val(startDate);
    $('#txtEndDate').val(endDate);
}

function ajaxGetList(GetPageInfo) {
	_PageSize=$('#sltPage').val();
	$('#ajaxDataList').hide();
	$('#ajaxDataLoading').show();

	$.ajax({
	    type: "GET", 
	    url: "../dataCount/qihooAppReport", 
	    data: "AjaxType=pagePageInfo&pageSize="+_PageSize+'&currentPage='+_CurrentPage
	    	+"&from="+$('#sltFrom').val()
	    	+"&appid="+$('#txtAppid').val()
	    	+"&className="+($('#txtClass').val().length>0?encodeURI($('#txtClass').val()):'')
	    	+"&owner="+$('#txtOwner').val()
	    	+"&timeBegin="+$('#txtStartDate').val()
	    	+"&timeEnd="+$('#txtEndDate').val()
	    	+"&province="+($('#sltProvince').val()==null?"-1":$('#sltProvince').val())
	    	+"&city="+($('#sltCity').val()==null?"-1":$('#sltCity').val())
	        +"&time=" + new Date(), 
	    success: function (result) {
	    	// alert(result['pageInfo']);
	    	if(GetPageInfo)
	    	{
	    		pageInfoDisplay(result['pageInfo']);
	    	}
		    // alert(result);
		     var html='';
			 if(result.pageData.length>0) 
			 {
				  if(result['summaryInfo'])
				  {
				  	html+='<tr><td colspan="3">应用总数：'+result['summaryInfo'].appCount+'</td>'
						//+'<td>'+result['summaryInfo'].showCount+'</td>'
						+'<td>'+result['summaryInfo'].downCount+'</td>'
						+'<td>'+result['summaryInfo'].installCount+'</td>'
						+'<td>'+result['summaryInfo'].activeCount+'</td></tr>';
				  }
			  	var items=result['pageData'];
			  	for(var i=0;i<items.length;i++)
			  	{
			  	 	html+='<tr>';
			    	html+='<td width="70">'+items[i].appid+'</td>';
			  	 	html+='<td class="auto">';
			  	 	html+='<a href="#" onclick="showInfoModal(\''+items[i].appid +'\')">'+items[i].appInfo.name +'</a></td>';
			    	html+='<td width="120">'+(items[i].appInfo?(items[i].appInfo.categoryName):'')+'</td>';	    
			    	//html+='<td>'+(items[i].goyooShowTimes?items[i].goyooShowTimes:'0')+'</td>'; 
			    	html+='<td width="120">'+items[i].goyooDownloadTimes+'</td>';  	
			    	html+='<td width="120">'+'0'+'</td>';  	
			    	html+='<td width="120">'+'0'+'</td>';
			    	html+='</tr>';
			  }
			}else
			{
	                html="<tr><td colspan='8'>未找到任何数据</td></tr>";
			}
			$('#ajaxDataList').html(html);
			$('#ajaxDataList').show();
			$('#ajaxDataLoading').hide();
		}
	});
}

function showInfoModal(id){
	 $.fancybox({
        'autoResize': true,
        'type': 'iframe',
        'width':750,
        'height':550,
		// 'afterClose':ajaxGetList,
        'href': '/qihooApps/appInfo/'+id        
    });
}
