var _PageSize=15;
var _pageInfo=true;

$(function(){
	pageInit();
    $('#btnSubmit').click(function () {_CurrentPage=1;ajaxGetList(); });
	// ajaxGetList();
	fixedLayer('inner');
})

function pageInit()
{
	$('#area').citySelect({
		url:'/city/',
	    prov:"",
	    city:"",
	    provPre:{text:"全部省份",value:"-1"},
	    cityPre:{text:"全部城市",value:"-1"}
	});

	var startDate=new Date().getFullYear()+ '-'
        + ('0' + (new Date().getMonth() + 1)).slice(-2)+ '-'
        + '01';
    var endDate=new Date().getFullYear()+ '-'
        + ('0' + (new Date().getMonth() + 1)).slice(-2)+ '-'
        + ('0' + (new Date().getDate())).slice(-2);
    $('#txtStartDate').val(startDate);
    $('#txtEndDate').val(endDate);
}

function ajaxGetList() {
	_PageSize=$('#sltPage').val();
	$('#ajaxDataList').hide();
	$('#ajaxDataLoading').show();
	$.ajax({
	    type: "GET", 
	    url: "../dataCount/onlineDayReport", 
	    data: "AjaxType=pagePageInfo&pageSize="+_PageSize+'&currentPage='+_CurrentPage
	    	+"&service="+encodeURI($('#stlService').val())
	    	+"&timeBegin="+$('#txtStartDate').val()
	    	+"&timeEnd="+$('#txtEndDate').val()
	    	+"&province="+($('#sltProvince').val()==null?"-1":$('#sltProvince').val())
	    	+"&city="+($('#sltCity').val()==null?"-1":$('#sltCity').val())
	    	+"&id="+$('#txtID').val()
	        +"&time=" + new Date(), 
	    success: function (result) {
	    	// alert(result['pageInfo']);
	    	if(_pageInfo)
	    	{
	    		pageInfoDisplay(result['pageInfo']);
	    	}
		    // alert(result);
		     var html="";
			 if(result.pageData.length>0) 
			 {
			  if(result['summaryInfo'])
			  {
			  	html+='<tr><td class="auto">总计：</td>'
					+'<td width="110"></td>'
					//+'<td width="110"></td>'
					+'<td width="110">'+result['summaryInfo'].devVisitorCount+'</td>'
					+'<td width="110">'+result['summaryInfo'].entryCount+'</td>'
					+'<td width="110">'+result['summaryInfo'].uvCount+'</td>'
					+'<td width="110">'+result['summaryInfo'].portal+'</td>'
					+'<td width="110">'+result['summaryInfo'].appCount+'</td>'
					+'<td width="110">'+(result['summaryInfo'].uvCount==0?'0':((result['summaryInfo'].appCount/result['summaryInfo'].uvCount).toFixed(2)))+'</td></tr>';
			  }

			  	var items=result['pageData'];
			  	for(var i=0;i<items.length;i++)
			  	{
					// <td><mark><span class="highlight">无</span></mark></td>

			  	 	html+='<tr>';
			    	html+='<td>'+items[i].day+'</td>';
			    	html+='<td>'+items[i].devicesCount+'</td>';
			    	//html+='<td>'+items[i].hightCount+'</td>';
			    	html+='<td>'+items[i].devVisitorCount+'</td>';
			    	html+='<td>'+items[i].entryCount+'</td>';
			    	html+='<td>'+items[i].uvCount+'</td>';
			    	html+='<td>'+items[i].portal+'</td>';
			    	html+='<td>'+items[i].appCount+'</td>';
			    	html+='<td>'+(items[i].uvCount==0?'0':((items[i].appCount/items[i].uvCount).toFixed(2)))+'</td>';
			    	html+='</tr>';

			  }
			}else
			{
	                html="<tr><td colspan='7'>未找到任何数据</td></tr>";
			}
			$('#ajaxDataList').html(html);
			$('#ajaxDataList').show();
			$('#ajaxDataLoading').hide();
		}
	});
}
