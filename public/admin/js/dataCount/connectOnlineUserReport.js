var _PageSize=15;
var _pageInfo=true;

$(function(){
	pageInit();
    $('#btnSubmit').click(function () {_CurrentPage=1;ajaxGetList(); });
	// ajaxGetList();
	fixedLayer('inner');
})

function pageInit()
{
	$('#area').citySelect({
		url:'/city/',
	    prov:"",
	    city:"",
	    provPre:{text:"全部省份",value:"-1"},
	    cityPre:{text:"全部城市",value:"-1"}
	});

	var startDate=new Date().getFullYear()+ '-'
        + ('0' + (new Date().getMonth() + 1)).slice(-2)+ '-'
        + '01';
    var endDate=new Date().getFullYear()+ '-'
        + ('0' + (new Date().getMonth() + 1)).slice(-2)+ '-'
        + ('0' + (new Date().getDate())).slice(-2);
    $('#txtStartDate').val(startDate);
    $('#txtEndDate').val(endDate);
}

function ajaxGetList() {
	_PageSize=$('#sltPage').val();
	$('#ajaxDataList').hide();
	$('#ajaxDataLoading').show();
	$.ajax({
	    type: "GET", 
	    url: "../dataCount/connectOnlineUserReport", 
	    data: "AjaxType=pagePageInfo&pageSize="+_PageSize+'&currentPage='+_CurrentPage
	    	+"&timeBegin="+$('#txtStartDate').val()
	    	+"&timeEnd="+$('#txtEndDate').val()
	        +"&time=" + new Date(), 
	    success: function (result) {
	    	// alert(result['pageInfo']);
	    	if(_pageInfo)
	    	{
	    		pageInfoDisplay(result['pageInfo']);
	    	}
		    // alert(result);
		     var html="";
			 if(result.pageData.length>0) 
			 {
			  if(result['summaryInfo'])
			  {
			  	html+='<tr><td class="auto">总计：</td>'
					+'<td width="150">'+result['summaryInfo'].connectUser+'</td>'
					+'<td width="150">'+result['summaryInfo'].connectNewUser+'</td>'
					+'<td width="150">'+result['summaryInfo'].netUser+'</td>'
					+'<td width="150">'+result['summaryInfo'].netNewUser+'</td>'
				}

			  	var items=result['pageData'];
			  	for(var i=0;i<items.length;i++)
			  	{
					// <td><mark><span class="highlight">无</span></mark></td>

			  	 	html+='<tr>';
			    	html+='<td class="auto">'+items[i].day+'</td>';
			    	html+='<td width="150">'+items[i].connectUser+'</td>';
			    	html+='<td width="150">'+items[i].connectNewUser+'</td>';
			    	html+='<td width="150">'+items[i].netUser+'</td>';
			    	html+='<td width="150">'+items[i].netNewUser+'</td>';
			    	html+='</tr>';

			  }
			}else
			{
	                html="<tr><td colspan='5'>未找到任何数据</td></tr>";
			}
			$('#ajaxDataList').html(html);
			$('#ajaxDataList').show();
			$('#ajaxDataLoading').hide();
		}
	});
}
