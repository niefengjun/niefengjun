/**
 * Created by wangdonghui on 14-1-14.
 */

var _PageSize = 15;
var _OrderDESC = false;

$(function () {
    $('#sltPage').change(function () {
        ajaxGetList(true);
    });
    $('#sType').change(function () {
        if (parseInt($(this).val()) != 1) {
            ajaxGetList(true);
        }
    });
    $('#btnSubmit').click(function () {
        _CurrentPage = 1;
        ajaxGetList(true);
    });
    ajaxGetList(true);
})

function ajaxGetList(GetPageInfo) {
    _PageSize = $('#sltPage').val();
    $('#ajaxDataList').hide();
    $('#ajaxDataLoading').show();
    $.ajax({
        type: "GET",
        url: "../devices/devicesApplyList",
        data: "AjaxType=getPageData&pageSize=" + _PageSize + '&currentPage=' + _CurrentPage + '&t=' + $('#sType').val() + '&k=' + $('#txtKey').val() + '&order=' + _OrderDESC
            + "&time=" + new Date(),
        success: function (result) {
            // alert(result['pageInfo']);
            if (GetPageInfo) {
                pageInfoDisplay(result['pageInfo']);
            }
            // alert(result);
            var html = "";
            if (result.pageData.length > 0) {
                var items = result['pageData'];
                for (var i = 0; i < items.length; i++) {
                    html += '<tr>';
                    html += '<td class="auto">' + items[i].orderID + '</td>';
                    html += '<td class="auto">' + items[i].channelID + '</td>';
                    html += '<td class="auto">' + items[i].company + '</td>';
                    html += '<td class="auto">' + items[i].area + '</td>';
                    html += '<td class="auto">' + items[i].person + '</td>';
                    html += '<td class="auto">' + items[i].devices.count + '</td>';
                    html += '<td class="auto">';
                    if (items[i].approvedDevices) {
                        html += (items[i].devices.count == items[i].approvedDevices.length ? items[i].approvedDevices.length : '<b style="color: #ff0000">' + items[i].approvedDevices.length + '</b>');
                    } else {
                        html += '<b style="color: #ff0000">0</b>';
                    }
                    html += '</td>';
                    html += '<td>' + convertTimestampToFullDate(items[i].created) + '</td>';
                    var status = "";
                    switch (items[i].status) {
                        case "applying":
                            status = "申请中";
                            break;
                        case "pass":
                            status = "已通过";
                            break;
                        case "noPass":
                            status = "未通过";
                            break;
                    }
                    html += '<td>' + status + '</td>';
                    if (status == "已通过" && items[i].approvedDevices && (items[i].approvedDevices.length == items[i].devices.count)) {
                        html += '<td><a href="../devices/devicesApplyInfo/' + items[i]._id + '">查看</a></td>';
                    }
                    else {
                        html += '<td><a href="../devices/devicesApplyInfo/' + items[i]._id + '">审批</a></td>';
                    }

                    html += '</tr>';
                }
            } else {
                html = "<tr><td colspan='10'>未找到任何数据</td></tr>";
            }
            $('#ajaxDataList').html(html);
            $('#ajaxDataList').show();
            $('#ajaxDataLoading').hide();
        }
    });
}

function orderByDate() {
    if (_OrderDESC) {
        _OrderDESC = false
    }
    else {
        _OrderDESC = true;
    }
    ajaxGetList(false);
}

function showModal(uuid) {
    $.fancybox({
        'autoResize': true,
        'type': 'iframe',
        'afterClose': ajaxGetList,
        'href': '../devices/devicesApplyInfo/' + uuid

    });
}