/**
 * Created by wangdonghui on 14-1-7.
 */
var _PageSize = 15;
var _OrderDESC = false;

$(function () {
    $('#sltPage').change(function () {
        ajaxGetList(true);
    });
    $('#sType').change(function () {
        if (parseInt($(this).val()) > 2) {
            ajaxGetList(true);
        }
    });
    $('#sDeviceType').change(function () {
        ajaxGetList(true);
    });
    $('#btnSubmit').click(function () {
        _CurrentPage = 1;
        ajaxGetList(true);
    });
    ajaxGetList(true);
})

function ajaxGetList(GetPageInfo) {
    _PageSize = $('#sltPage').val();
    $('#ajaxDataList').hide();
    $('#ajaxDataLoading').show();
    $.ajax({
        type: "GET",
        url: "../devices/",
        data: "AjaxType=getPageData&pageSize=" + _PageSize + '&currentPage=' + _CurrentPage + '&t=' + $('#sType').val() + '&k=' + $('#txtKey').val() + '&order=' + _OrderDESC
            + "&time=" + new Date(),
        success: function (result) {
            // alert(result['pageInfo']);
            if (GetPageInfo) {
                pageInfoDisplay(result['pageInfo']);
            }
            // alert(result);
            var html = "";
            if (result.pageData.length > 0) {
                var items = result['pageData'];
                for (var i = 0; i < items.length; i++) {
                    if ($("#sDeviceType").val() != "all" && $("#sDeviceType").val() != items[i].deviceStatus) {
                        continue;
                    }
                    // <td><mark><span class="highlight">无</span></mark></td>
                    // alert(items[i]);
                    html += '<tr>';
                    html += '<td class="auto">' + items[i].sn + '</td>';
                    html += '<td class="auto">' + items[i].apmac + '</td>';
                    html += '<td>' + convertTimestampToFullDate(items[i].created) + '</td>';
                    switch (items[i].status) {
                        case "in":
                            html += '<td>在库</td>';
                            break;
                        case "out":
                            html += '<td>出库</td>';
                            break;
                        case "used":
                            html += '<td>绑定</td>';
                            break;
                        default:
                            html += '<td></td>';
                            break;
                    }
                    if (items[i].deviceStatus) {
                        switch (items[i].deviceStatus) {
                            case "test":
                                html += '<td>测试设备</td>';
                                break;
                            case "deposit":
                                html += '<td>保证金设备</td>';
                                break;
                            case "sell":
                                html += '<td>销售设备</td>';
                                break;
                            case "internalFriction":
                                html += '<td>内耗设备</td>';
                                break;
                            case "channelUnbind":
                                html += '<td>渠道解绑设备</td>';
                                break;
                            default:
                                html += '<td></td>';
                                break;
                        }
                    }
                    else {
                        html += '<td>库存设备</td>';
                    }
                    html += '<td>' + items[i].createdBy + '</td>';
                    if (items[i].status == "in") {
                        html += '<td></td>';
//                        html += '<td><a href="javascript:;" onclick="deviceOut(\'' + items[i]._id + '\')">出库</a></td>';
                    }
                    else {
                        html += '<td><a href="/devices/devicesInfo?mac=' + items[i].apmac + '">详情</a></td>';
                    }
                    html += '</tr>';
                }
            } else {
                html = "<tr><td colspan='10'>未找到任何数据</td></tr>";
            }
            $('#ajaxDataList').html(html);
            $('#ajaxDataList').show();
            $('#ajaxDataLoading').hide();
        }
    })
    ;
}

function orderByDate() {
    if (_OrderDESC) {
        _OrderDESC = false
    }
    else {
        _OrderDESC = true;
    }
    ajaxGetList(false);
}

function deviceOut(uuid) {
    alert(uuid);
}

function deviceInfo(mac) {
    alert(mac);
}