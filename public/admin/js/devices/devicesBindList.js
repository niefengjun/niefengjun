/**
 * Created by wangdonghui on 14-2-24.
 */
var _PageSize = 15;
var _OrderDESC = false;

$(function () {
    $('#sltPage').change(function () {
        ajaxGetList(true);
    });
    $('#sType').change(function () {
        if (parseInt($(this).val()) == 0) {
            ajaxGetList(true);
        }
    });
    $('#area').citySelect({
        url: '/city/',
        prov: "",
        city: "",
        provPre: {text: "全部省份", value: "-1"},
        cityPre: {text: "全部城市", value: "-1"}
    });
    $('#btnSubmit').click(function () {
        _CurrentPage = 1;
        ajaxGetList(true);
    });
    ajaxGetList(true);
})


function ajaxGetList(GetPageInfo) {
    _PageSize = $('#sltPage').val();
    $('#ajaxDataList').hide();
    $('#ajaxDataLoading').show();

    $.ajax({
        type: "GET",
        url: "../devices/devicesBindList",
        data: "AjaxType=getPageData&pageSize=" + _PageSize
            + '&currentPage=' + _CurrentPage + '&t=0&k=0'

            + '&order=' + _OrderDESC + "&time=" + new Date()
            + "&province=" + ($('#sltProvince').val() == null ? "-1" : $('#sltProvince').val())
            + "&ids=" + ($('#ids').val().length==0 ? "-1" : $('#ids').val())
            + "&shopid=" + ($('#shopid').val().length==0  ? "-1" : $('#shopid').val())
            + "&devicesn=" + ($('#devicesn').val().length==0  ? "-1" : $('#devicesn').val())
            + "&devicemac=" + ($('#devicemac').val().length==0  ? "-1" : $('#devicemac').val())
            + "&city=" + ($('#sltCity').val() == null ? "-1" : $('#sltCity').val())
            + "&versions=" + $('#slversions').val(),

        success: function (result) {
            // alert(result['pageInfo']);
            if (GetPageInfo) {
                pageInfoDisplay(result['pageInfo']);
            }
            // alert(result);
            var html = "";
            if (result.pageData.length > 0) {
                var items = result['pageData'];
                for (var i = 0; i < items.length; i++) {
                    if (items[i]&&items[i].sn) {


                        html += '<tr>';
                        html += '<td class="auto" >' + items[i].sn + '</td>';
                        html += '<td class="auto">' + items[i].mac + '</td>';
                        var channelID = "", company = "", unbindChannel = "";
                        var area = "";
                        var profession = '';
                        if (items[i].channelInfo) {
                            channelID = items[i].channelInfo.channelID;
                            company = items[i].channelInfo.company;
                            area = items[i].channelInfo.area;
                            profession = items[i].channelInfo.profession;
                            unbindChannel = "<a href='javascript:void(0);' onclick='unbindChannel(\"" + (items[i].locationInfo ? items[i].locationInfo._id : undefined) + "\",\"" + items[i]._id + "\",\"channel\",\"" + items[i].channelInfo._id + "\")'>解绑</a>"
                        }
                        html += "<td class='auto'>" + channelID + "</td>";
                        html += "<td class='auto'>" + company + unbindChannel + "</td>";

                        html += "<td class='auto'>" + area + "</td>";


//                     html += "<td class='auto'>" + items[i].online+"</td>";


                        /*html += '<td class="auto" >' + (items[i].channelInfo ? items[i].channelInfo.channelID : "") + '</td>';
                         html += '<td class="auto" >' + (items[i].channelInfo ? items[i].channelInfo.company : "") + '</td>';*/
                        var ids = "", nickName = "", unbindShop = "";
                        if (items[i].userInfo) {
                            ids = items[i].userInfo.ids[0];
                            nickName = items[i].userInfo.nickName;
                            unbindShop = "<a href='javascript:void(0);' onclick='unbindShop(\"" + (items[i].locationInfo ? items[i].locationInfo._id : undefined) + "\",\"" + items[i]._id + "\",\"shop\",\"" + items[i].userInfo._id + "\")'>解绑</a>"
                        }
                        html += "<td class='auto'>" + ids + "</td>";
                        html += "<td class='auto'>" + nickName + unbindShop + "</td>";
                        /*html += '<td class="auto" >' + (items[i].userInfo ? (items[i].userInfo.ids ? items[i].userInfo.ids[0] : "") : "") + '</td>';
                         html += '<td class="auto" >' + (items[i].userInfo ? items[i].userInfo.nickName : "") + '</td>';*/
                        html += '<td class="auto" >' + (items[i].locationInfo ? items[i].locationInfo.title + ' <a href=\"javascript:void(0);\" onclick=\"unbindLocation(\'' + items[i].locationInfo._id + '\',\'' + items[i]._id + '\',\'shop\')\">解绑</a>' : "") + '</td>';
                        html += "<td class='auto'>" + items[i].version + "</td>";

                        if (items[i].update == '1') {
                            html += "<td ><a href='javascript:void(0);' onclick='getupdate(\"" + items[i].mac + "\" )'>版本</a> ||   <a href='javascript:void(0);' onclick='uponline(\"" + items[i].mac + "\" )'>在线</a>||<a href='javascript:void(0);' onclick='update(\"" + items[i].mac + "\" )'>升级</a></td>";
                        }
                        else {
                            html += "<td ><a href='javascript:void(0);' onclick='getupdate(\"" + items[i].mac + "\" )'>版本</a> || <a href='javascript:void(0);' onclick='uponline(\"" + items[i].mac + "\" )'>在线</a>||<a href='javascript:void(0);' onclick='update(\"" + items[i].mac + "\" )'>已升级</a></td>";
                        }
                        html += '</tr>';
                    }
                }
            } else {
                html = "<tr><td colspan='10'>未找到任何数据</td></tr>";
            }
            $('#ajaxDataList').html(html);
            $('#ajaxDataList').show();
            $('#ajaxDataLoading').hide();
        }
    });
}

function orderByDate() {
    if (_OrderDESC) {
        _OrderDESC = false
    }
    else {
        _OrderDESC = true;
    }
    ajaxGetList(false);
}

function showModal(uuid) {
    $.fancybox({
        'autoResize': true,
        'type': 'iframe',
        'afterClose': ajaxGetList,
        'href': '../devices/devicesApplyInfo/' + uuid

    });
}
function unbindLocation(locationID, deviceID, role) {
    if (confirm("确定要解绑商铺吗？")) {
        $.ajax({
            type: "POST",
            url: "../devices/unbindDeviceForLocation",
            data: {locationID: locationID, deviceID: deviceID, role: role},
            dataType: 'JSON',
            success: function (result) {
                if (result.status == "0") {
                    ajaxGetList(true);
                }
                else {
                    alert(result.msg);
                }
            }
        });
    }
}


function unbindShop(locationID, deviceID, role, userID) {
    if (confirm("确定要解绑商户吗？")) {
        $.ajax({
            type: "POST",
            url: "../devices/deleteDevice",
            data: {locationID: locationID, deviceID: deviceID, role: role, userID: userID},
            dataType: 'JSON',
            success: function (result) {
                if (result.status == "0") {
                    ajaxGetList(true);
                }
                else {
                    alert(result.msg);
                }
            }
        });
    }
}
function unbindChannel(locationID, deviceID, role, userID) {
    if (confirm("确定要解绑渠道吗？")) {
        $.ajax({
            type: "POST",
            url: "../devices/deleteDevice",
            data: {locationID: locationID, deviceID: deviceID, role: role, userID: userID, deviceWareHouseStatus: 'channelUnbind'},
            dataType: 'JSON',
            success: function (result) {
                if (result.status == "0") {
                    ajaxGetList(true);
                }
                else {
                    alert(result.msg);
                }
            }
        });
    }
}


function getupdate(mac) {
    if (confirm("确认获取设备版本吗？")) {
        $.ajax({
            type: "get",
            url: "../devices/devicesBindList",
            data: "AjaxType=getDevicesVersion&mac=" + mac + "&time=" + new Date(),
            success: function (err) {
                if (err.code == '0') {
                    alert('获取设备版本成功');
                    ajaxGetList(true);
                    return  false;
                }
                else if (err.code == '-2') {
                    alert('设备不在线');
                    return  false;
                }
                else if (err.code == '-1') {
                    alert('设备不存在 ');
                    return  false;
                }
                else {
                    alert('获取设备版本失败' + err.msg);
                    return  false;
                }
            }
        });
    }
}

function update(mac) {
    if (confirm("确定要升级此设备吗？")) {
        $.ajax({
            type: "get",
            url: "../devices/devicesBindList",
            data: "AjaxType=divicesupdate&mac=" + mac + "&time=" + new Date(),
            success: function (err) {
                if (err.code == '0') {
                    alert('升级命令已下发 将在几分钟之内完成升级 请稍等');
                    return  false;
                }
                else {
                    alert('升级失败' + err.msg);
                    return  false;
                }
            }
        });
    }
}
function uponline(mac) {

    $.ajax({
        type: "get",
        url: "/api/clients",
        data: "mac=" + mac + "&time=" + new Date(),
        success: function (result) {
            if (result == '0') {
                alert(' 在线');
                return  false;
            }
            else {
                alert('下线');
                return  false;
            }
        }
    });

}
