var _PageSize=15;
var _pageInfo=true;

$(function(){
	$('#area').citySelect({
		url:'../city/',
	    prov:"",
	    city:"",
	    provPre:{text:"全部省份",value:"-1"},
	    cityPre:{text:"全部城市",value:"-1"}
	});
    $('#btnSubmit').click(function () {_CurrentPage=1;ajaxGetList(); });
	ajaxGetList();
})

function ajaxGetList() {
	_PageSize=$('#sltPage').val();
	$('#ajaxDataList').hide();
	$('#ajaxDataLoading').show();
	$.ajax({
	    type: "GET", 
	    url: "../location/shopList", 
	    data: "AjaxType=pagePageInfo&pageSize="+_PageSize+'&currentPage='+_CurrentPage
	    	+"&email="+$('#txtEmail').val()
	    	+"&title="+escape($('#txtTitle').val())
	    	+"&status="+$('#sltStatus').val()
	    	+"&province="+($('#sltProvince').val()==null?"-1":$('#sltProvince').val())
	    	+"&city="+($('#sltCity').val()==null?"-1":$('#sltCity').val())
	        +"&time=" + new Date(), 
	    success: function (result) {
	    	// alert(result['pageInfo']);
	    	if(_pageInfo)
	    	{
	    		pageInfoDisplay(result['pageInfo']);
	    	}
		    // alert(result);
		     var html="";
			 if(result.pageData.length>0) 
			 {
			  	var items=result['pageData'];
			  	for(var i=0;i<items.length;i++)
			  	{
					// <td><mark><span class="highlight">无</span></mark></td>

			  	 	html+='<tr>';
			    	html+='<td>'+items[i].email+'</td>';
			  	 	html+='<td class="auto">';
			  	 	html+='<a href="#" onclick="showModal(\''+items[i]._id +'\')">'+items[i].title +'</a></td>';
			    	html+='<td>'+(items[i].length?items[i].length:0)+'</td>';
			    	html+='<td>'+items[i].contacts+'</td>';
			    	html+='<td>'+items[i].area+'</td>';
			    	if(items[i].verified==false||items[i].verified=='false'||items[i].verified==undefined)
			    	{
			    		html+='<td>待审核</td>';
			    	}else if(items[i].verified==true||items[i].verified=='true')
			    	{
			    		html+='<td>已通过</td>';
			    	}else
			    	{
			    		html+='<td>未通过</td>';
			    	}
			    	html+='<td><a href="#" onclick="showModal(\''+items[i]._id +'\')">查看</a></td>';
			    	html+='</tr>';

			  }
			}else
			{
	                html="<tr><td colspan='7'>未找到任何数据</td></tr>";
			}
			$('#ajaxDataList').html(html);
			$('#ajaxDataList').show();
			$('#ajaxDataLoading').hide();
		}
	});
}
function showModal(uuid){
	 $.fancybox({
        'autoResize': true,
        'type': 'iframe',
        'width':750,
        'height':550,
		'afterClose':ajaxGetList,
        'href': '/location/shopInfo/'+uuid        
    });
}