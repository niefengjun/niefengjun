
function changeTab(aTab,uuid)
{
	$('#ulTab li').removeClass('selected');
	$(aTab).parent('li').addClass('selected');
	ajaxGetList(uuid);
}

function ajaxGetList(uuid) {
	$('#ajaxDataList').hide();
	$('#ajaxDataLoading').show();
	$.ajax({
	    type: "POST", 
	    url: "../../location/shopCount", 
	    data: {uuid:uuid}, 
	    success: function (result) {
	    	if(result.location)
	    	{
	    		$("#strTitle").html(result.location.title);
	    		$("#strCreated").html(result.location.created);
	    		$("#strContacts").html(result.location.contacts);
	    		$("#strServices").html(result.location.services);
	    		$("#strAvg").html(result.location.avgConsume);
	    	}else
	    	{
	    		$("#strTitle").html('');
	    		$("#strCreated").html('');
	    		$("#strContacts").html('');
	    		$("#strServices").html('');
	    		$("#strAvg").html('');
	    	}

		     var html="";
			 if(result.devices.length>0) 
			 {
			  	var items=result.devices;
			  	for(var i=0;i<items.length;i++)
			  	{
			  	 	html+='<tr>';
			    	html+='<td>'+items[i].sn+'</td>';
			    	html+='<td>'+items[i].mac+'</td>';
			    	html+='<td>'+items[i].created+'</td>';
			    	html+='<td>'+items[i].online+'</td>';
			    	html+='</tr>';
				}
			}else
			{
	                html="<tr><td colspan='4'>未找到任何数据</td></tr>";
			}
			$('#ajaxDataList').html(html);
			$('#ajaxDataList').show();
			$('#ajaxDataLoading').hide();
		}
	});
}
