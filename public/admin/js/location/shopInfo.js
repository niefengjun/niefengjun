
$(function(){
	$("#btnSubmit").click(function(){
      	postShopInfo();
    })

    if ($('#coordinates').val() != '') {
        var coordinates = $('#coordinates').val();
        var lng = parseFloat(coordinates.split(',')[0]);
        var lat = parseFloat(coordinates.split(',')[1]);
        mapInit(lng, lat);
    }    
})

function changeTab(index)
{
	$('#li0').removeClass('selected');
	$('#li1').removeClass('selected');
    $('#li2').removeClass('selected');
    $('#li3').removeClass('selected');
	$('#li'+index).addClass('selected');
	$('#mod0').hide();
	$('#mod1').hide();
    $('#mod2').hide();
    $('#mod3').hide();
	$('#mod'+index).show();
}

function postShopInfo(){
	var nopass=$("#nopasstxt").val()==''?'nopass':$("#nopasstxt").val();
	var str='';
      $("input[name=work]:checked").each(function(i){
        str+=$(this).val()+',';
      })
      str=str.substring(0,str.length-1);

      $.ajax({
        type:'post',
        url:'/location/shopInfo',
        data:{"authType":"true","uuid":$("#uuid").val(),"txt":nopass},
        dataType:'json',
        success:function(data){
          if(data.status=='success'){
            alert('审核成功');
          }else{
            //提示
            alert(data.msg);
          }
        }
    })
}

function mapInit(lng, lat) {
	var map = new BMap.Map("map");
	map.centerAndZoom(new BMap.Point(lng, lat), 11);
	var marker1 = new BMap.Marker(new BMap.Point(lng, lat));  // 创建标注
	map.addOverlay(marker1);
}