var _PageSize=15;
var _pageInfo=true;

$(function(){
    $('#btnSubmit').click(function () {_CurrentPage=1;ajaxGetList(); });
	ajaxGetList();
})

function ajaxGetList() {
	_PageSize=$('#sltPage').val();
	$('#ajaxDataList').hide();
	$('#ajaxDataLoading').show();
	$.ajax({
	    type: "GET", 
	    url: "../location/shopAuthList", 
	    data: "AjaxType=pagePageInfo&pageSize="+_PageSize+'&currentPage='+_CurrentPage
	    	+"&email="+$('#txtEmail').val()
	    	+"&title="+escape($('#txtTitle').val())
	    	+"&status="+$('#sltStatus').val()
	        +"&time=" + new Date(), 
	    success: function (result) {
	    	// alert(result['pageInfo']);
	    	if(_pageInfo)
	    	{
	    		pageInfoDisplay(result['pageInfo']);
	    	}
		    // alert(result);
		     var html="";
			 if(result.pageData.length>0) 
			 {
			  	var items=result['pageData'];
			  	for(var i=0;i<items.length;i++)
			  	{
					// <td><mark><span class="highlight">无</span></mark></td>

			  	 	html+='<tr>';
			    	html+='<td>'+items[i].email+'</td>';
			  	 	html+='<td class="auto">';
			  	 	html+='<a href="#" onclick="showModal(\''+items[i]._id +'\')">'+items[i].title +'</a></td>';
			    	html+='<td>'+items[i].created+'</td>';
			    	html+='<td>'+items[i].owner+'</td>';
			    	html+='<td>'+items[i].contacts+'</td>';
			    	html+='<td>'+items[i].services+'</td>';
			    	if(items[i].verified==false||items[i].verified=='false'||items[i].verified==undefined)
			    	{
			    		html+='<td>待审核</td>';
			    	}else if(items[i].verified==true||items[i].verified=='true')
			    	{
			    		html+='<td>已通过</td>';
			    	}else
			    	{
			    		html+='<td>未通过</td>';
			    	}
			    	html+='<td><a href="#" onclick="showModal(\''+items[i]._id +'\')">审核</a></td>';
			    	html+='</tr>';

			  }
			}else
			{
	                html="<tr><td colspan='8'>未找到任何数据</td></tr>";
			}
			$('#ajaxDataList').html(html);
			$('#ajaxDataList').show();
			$('#ajaxDataLoading').hide();
		}
	});
}
function showModal(uuid){
	 $.fancybox({
        'autoResize': true,
        'type': 'iframe',
        'width':750,
        'height':550,
		'afterClose':ajaxGetList,
		'autoSize':true,
        'href': '/location/shopAuth/'+uuid        
    });
}
