$(function () {
    $("#btnPass").click(function () {
        passAuthInfo();
    })

    $("#btnNopass").click(function () {
        nopassAuthInfo();
    })

    if (coordinates != '') {
        var coordinates = $('#coordinates').val();
        var lng = parseFloat(coordinates.split(',')[0]);
        var lat = parseFloat(coordinates.split(',')[1]);
        mapInit(lng, lat);
    }
})

function changeTab(index) {
    $('#li0').removeClass('selected');
    $('#li1').removeClass('selected');
    $('#li2').removeClass('selected');
    $('#li3').removeClass('selected');
    $('#li4').removeClass('selected');
    $('#li' + index).addClass('selected');
    $('#mod0').hide();
    $('#mod1').hide();

    $('#mod2').hide();
    $('#mod3').hide();
    $('#mod4').hide();
    $('#mod' + index).show();
}

function passAuthInfo() {
    var nopass = $("#nopasstxt").val() == '' ? 'nopass' : $("#nopasstxt").val();
    var str = '';
    $("input[name=work]:checked").each(function (i) {
        str += $(this).val() + ',';
    })
    str = str.substring(0, str.length - 1);

    $.ajax({
        type: 'post',
        url: '/location/shopAuth',
        data: {"authType": "true", "uuid": $("#uuid").val(), "txt": nopass},
        dataType: 'json',
        success: function (data) {
            if (data.status == 'success') {
                alert('审核成功');
            } else {
                //提示
                alert(data.msg);
            }
        }
    })
}

function nopassAuthInfo() {
    var nopass = $("#nopasstxt").val() == '' ? 'nopass' : $("#nopasstxt").val();
    $.ajax({
        type: 'post',
        url: '/location/shopAuth',
        data: {"authType": "false", "uuid": $("#uuid").val(), "txt": nopass},
        dataType: 'json',
        success: function (data) {
            if (data.status == 'success') {
                //提示,关闭弹出框
                alert('审核成功');
            } else {
                //提示
                alert(data.msg);
            }
        }
    })
}

function mapInit(lng, lat) {
    var map = new BMap.Map("map");
    map.centerAndZoom(new BMap.Point(lng, lat), 11);
    var marker1 = new BMap.Marker(new BMap.Point(lng, lat));  // 创建标注
    map.addOverlay(marker1);
}