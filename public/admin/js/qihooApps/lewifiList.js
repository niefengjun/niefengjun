var _PageSize = 15;

$(function () {
    $('#btnSearch').click(function () {
        _CurrentPage = 1;
        ajaxGetList(true);
    });

    //全选
    $("#checkAll").click(function () {
        if ($(this).is(":checked")) {
            $("input[type='checkbox']").attr("checked", "checked");
        }
        else {
            $("input[type='checkbox']").removeAttr("checked");
        }
    });
    ajaxGetList(true);
})

function ajaxGetList(GetPageInfo) {
    _PageSize = $('#sltPage').val();
    $('#ajaxDataList').hide();
    $('#ajaxDataLoading').show();

    $.ajax({
        type: "GET",
        url: "../qihooApps/lewifiList",
        data: "AjaxType=pagePageInfo&pageSize=" + _PageSize + '&currentPage=' + _CurrentPage
            + "&type=" + $('#sltType').val()
            + "&time=" + new Date(),
        success: function (result) {
            // alert(result['pageInfo']);
            if (GetPageInfo) {
                pageInfoDisplay(result['pageInfo']);
            }
            // alert(result);
            var html = "";
            if (result.pageData.length > 0) {
                var items = result['pageData'];
                for (var i = 0; i < items.length; i++) {
                    html += '<tr>';
                    html += '<td class="chk"><input type="checkbox" value="' + items[i].id + '"/></td>';
                    html += '<td>' + items[i].id + '</td>';
                    html += '<td class="auto">';

                    if (items[i].top == 1) {
                        html += '<a href="#" onclick="showInfoModal(\'' + items[i].id + '\')">' + items[i].name + '</a>[置顶]</td>';
                    }
                    else {
                        html += '<a href="#" onclick="showInfoModal(\'' + items[i].id + '\')">' + items[i].name + '</a></td>';
                    }

                    html += '<td>' + items[i].categoryName + '</td>';
                    html += '<td>' + (items[i].incomeShare == 1 ? '是' : '否') + '</td>';
                    html += '<td>' + items[i].developer + '</td>';
                    html += '<td>' + items[i].versionName + '</td>';
                    html += '<td>' + items[i].downloadTimes + '\\' + (items[i].goyooDownloadTimes ? items[i].goyooDownloadTimes : 0) + '</td>';
                    if (items[i].top == 1) {
                        html += '<td><a href="#" onclick="lewifiDataDelete(\'' + items[i].id + '\')">删除</a>||<a href="#" onclick="lewifitop(\'' + items[i].id + '\',0)">取消置顶</a></td>';
                    }
                    else {
                        html += '<td><a href="#" onclick="lewifiDataDelete(\'' + items[i].id + '\')">删除</a>||<a href="#" onclick="lewifitop(\'' + items[i].id + '\',1)">置顶</a></td>';
                    }
                    html += '</tr>';
                }
            } else {
                html = "<tr><td colspan='9'>未找到任何数据</td></tr>";
            }
            $('#ajaxDataList').html(html);
            $('#ajaxDataList').show();
            $('#ajaxDataLoading').hide();
        }
    });
}

function showInfoModal(id) {
    $.fancybox({
        'autoResize': true,
        'type': 'iframe',
        'width': 750,
        'height': 550,
        // 'afterClose':ajaxGetList,
        'href': '/qihooApps/appInfo/' + id
    });
}

function lewifiDataDelete(appid) {
    _PageSize = $('#sltPage').val();
    $('#ajaxDataList').hide();
    $('#ajaxDataLoading').show();

    $.ajax({
        type: "GET",
        url: "../qihooApps/lewifiApiDelete",
        data: "&appid=" + appid
            + "&time=" + new Date(),
        success: function (result) {
            var ret = result.split('|');
            if (ret[0] == '0') {
                alert(ret[1]);
            } else {
                // alert(ret[1]);
                ajaxGetList(true);
            }
        }
    });
}


function lewifitop(appid, v) {
    _PageSize = $('#sltPage').val();
    $('#ajaxDataList').hide();
    $('#ajaxDataLoading').show();

    $.ajax({
        type: "GET",
        url: "../qihooApps/lewifiList",
        data: "AjaxType=puttopapps&v=" + v + "&_id=" + appid
            + "&time=" + new Date(),
        success: function (result) {

            if (result == '0') {
                alert('设置失败');

            } else {

                alert('设置成功');
                ajaxGetList(true);
            }
        }
    });
}