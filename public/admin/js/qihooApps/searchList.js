var _PageSize=15;
var _searchType=1;

$(function(){
    $('#btnSearch1').click(function () {_CurrentPage=1;_searchType=1;ajaxGetList(true); });
    $('#btnSearch2').click(function () {_CurrentPage=1;_searchType=2;ajaxGetList(true); });
    $('#btnToLewifi').click(function () {showTolewifiModal();});

    $('#sltType').change(function(){
		categoryBind();
    });
    //全选
	$("#checkAll").click(function () {
		if ($(this).is(":checked")) {
			$("input[type='checkbox']").attr("checked", "checked");
		}
		else {
			$("input[type='checkbox']").removeAttr("checked");
		}
	});
	ajaxGetList(true);
})

function ajaxGetList(GetPageInfo) {
	_PageSize=$('#sltPage').val();
	$('#ajaxDataList').hide();
	$('#ajaxDataLoading').show();

	if(_searchType==1)
	{
		$.ajax({
		    type: "GET", 
		    url: "../qihooApps/searchList", 
		    data: "AjaxType=applist&pageSize="+_PageSize+'&currentPage='+_CurrentPage
		    	+"&type="+$('#sltType').val()
		    	+"&isSdk="+$('#sltIsSdk').val()
		        +"&time=" + new Date(), 
		    success: function (result) {
		    	// alert(result['pageInfo']);
		    	if(GetPageInfo)
		    	{
		    		pageInfoDisplay(result['pageInfo']);
		    	}
			    // alert(result);
			     var html="";
				 if(result.pageData.length>0) 
				 {
				  	var items=result['pageData'];
				  	for(var i=0;i<items.length;i++)
				  	{
				  	 	html+='<tr>';
				    	html+='<td class="chk"><input type="checkbox" value="'+items[i].id+'"/></td>';
				    	html+='<td>'+items[i].id+'</td>';
				  	 	html+='<td class="auto">';
				  	 	html+='<a href="#" onclick="showInfoModal(\''+items[i].id +'\')">'+items[i].name +'</a></td>';			    	
				    	html+='<td>'+items[i].categoryName+'</td>';
				    	html+='<td>'+(items[i].incomeShare==1?'是':'否')+'</td>';			    	
				    	html+='<td>'+items[i].developer+'</td>';
				    	html+='<td>'+items[i].versionName+'</td>';
				    	html+='<td>'+items[i].downloadTimes+'</td>';
				    	html+='<td><a href="#" onclick="showAppPostModal(\''+items[i].id +'\')">推送</a></td>';
				    	html+='</tr>';
				  }
				}else
				{
		                html="<tr><td colspan='9'>未找到任何数据</td></tr>";
				}
				$('#ajaxDataList').html(html);
				$('#ajaxDataList').show();
				$('#ajaxDataLoading').hide();
			}
		});

	}else if(_searchType==2){
		if($('#txtID').val().length==0)
		{
			alert('请输入应用ID');
			return;
		}
		$.ajax({
		    type: "GET", 
		    url: "../qihooApps/searchList", 
		    data: "AjaxType=getapp&pageSize="+_PageSize+'&currentPage='+_CurrentPage
		    	+"&appid="+$('#txtID').val()
		        +"&time=" + new Date(), 
		    success: function (result) {
		    	// alert(result['pageInfo']);
		    	if(GetPageInfo)
		    	{
		    		pageInfoDisplay(result['pageInfo']);
		    	}
			    // alert(result);
			     var html="";
				 if(result.pageData.length>0) 
				 {
				  	var items=result['pageData'];
				  	for(var i=0;i<items.length;i++)
				  	{
				  	 	html+='<tr>';
				    	html+='<td class="chk"><input type="checkbox" value="'+items[i].id+'"/></td>';
				    	html+='<td>'+items[i].id+'</td>';
				  	 	html+='<td class="auto">';
				  	 	html+='<a href="#" onclick="showInfoModal(\''+items[i].id +'\')">'+items[i].name +'</a></td>';			    	
				    	html+='<td>'+items[i].categoryName+'</td>';
				    	html+='<td>'+(items[i].incomeShare==1?'是':'否')+'</td>';			    	
				    	html+='<td>'+items[i].developer+'</td>';
				    	html+='<td>'+items[i].versionName+'</td>';
				    	html+='<td>'+items[i].downloadTimes+'</td>';
				    	html+='<td><a href="#" onclick="showAppPostModal(\''+items[i].id +'\')">推送</a></td>';
				    	html+='</tr>';
				  }
				}else
				{
		                html="<tr><td colspan='9'>未找到任何数据</td></tr>";
				}
				$('#ajaxDataList').html(html);
				$('#ajaxDataList').show();
				$('#ajaxDataLoading').hide();
			}
		});
	}
}

function showInfoModal(id){
	 $.fancybox({
        'autoResize': true,
        'type': 'iframe',
        'width':750,
        'height':550,
		// 'afterClose':ajaxGetList,
        'href': '/qihooApps/appInfo/'+id        
    });
}

function showTolewifiModal(){
	if ($("#ajaxDataList input[type='checkbox']:checked").length > 0) {
		var _id = "";
		$("#ajaxDataList input[type='checkbox']:checked").each(function (i, c) {
			_id += $(this).val() + ",";
		});
		_id = _id.substring(0, _id.length - 1);

		$.fancybox({
	        'autoResize': true,
	        'type': 'iframe',
	        'width':500,
	        'height':400,
			// 'afterClose':ajaxGetList,
	        'href': '/qihooApps/appToLewifi/'+_id        
	    });
	}
	else {
		alert("未选择任何数据");
	}
}

function showAppPostModal(_id){
	$.fancybox({
        'autoResize': true,
        'type': 'iframe',
        'width':500,
        'height':400,
		// 'afterClose':ajaxGetList,
        'href': '/qihooApps/appPost/'+_id        
    });
}

function categoryBind(){
	$.ajax({
	    type: "GET", 
	    url: "/qihooApps/getCatItems", 
	    data: "time=" + new Date(), 
	    success: function (result) {	      
	    	var html='<option value="-1">全部类别</option>';
    		var type='soft';
    		if($("#sltType").val()=='1')
    			type='soft';
    		if($("#sltType").val()=='2')
    			type='game';
	    	if($("#sltType").val()!='-1'&&result[type])
	    	{
		    	for (var i = 0; i < result[type].length; i++) {
		    		html+='<option value="'+result[type][i]+'">'+result[type][i]+'</option>';
		    	};
	    	}
	    	$('#sltCat').html(html);
	    }
	});
}
