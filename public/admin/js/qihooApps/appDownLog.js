var _PageSize=15;

$(function(){
    $('#btnSearch').click(function () {_CurrentPage=1;ajaxGetList(true); });

    //全选
	$("#checkAll").click(function () {
		if ($(this).is(":checked")) {
			$("input[type='checkbox']").attr("checked", "checked");
		}
		else {
			$("input[type='checkbox']").removeAttr("checked");
		}
	});
	ajaxGetList(true);
})

function ajaxGetList(GetPageInfo) {
	_PageSize=$('#sltPage').val();
	$('#ajaxDataList').hide();
	$('#ajaxDataLoading').show();

	$.ajax({
	    type: "GET", 
	    url: "../qihooApps/appDownLog", 
	    data: "AjaxType=pagePageInfo&pageSize="+_PageSize+'&currentPage='+_CurrentPage
	    	+"&from="+$('#sltFrom').val()
	    	+"&appid="+$('#txtAppid').val()
	        +"&time=" + new Date(), 
	    success: function (result) {
	    	// alert(result['pageInfo']);
	    	if(GetPageInfo)
	    	{
	    		pageInfoDisplay(result['pageInfo']);
	    	}
		    // alert(result);
		     var html="";
			 if(result.pageData.length>0) 
			 {
			  	var items=result['pageData'];
			  	for(var i=0;i<items.length;i++)
			  	{
			  	 	html+='<tr>';
			    	html+='<td class="chk"><input type="checkbox" value="'+items[i].appid+'"/></td>';
			    	html+='<td>'+items[i].appid+'</td>';
			  	 	html+='<td class="auto">';
			  	 	html+='<a href="#" onclick="showInfoModal(\''+items[i].appid +'\')">'+items[i].appName +'</a></td>';
			    	html+='<td>'+items[i].channelName+'</td>';
			    	html+='<td>'+items[i].locationName+'</td>';
			    	html+='<td>'+items[i].shopName+'</td>';
			    	html+='<td>'+items[i].from+'</td>';	    	
			    	// html+='<td>'+items[i].downloadIp+'</td>';
			    	html+='<td>'+items[i].downloadTime+'</td>';
			    	html+='</tr>';
			  }
			}else
			{
	                html="<tr><td colspan='9'>未找到任何数据</td></tr>";
			}
			$('#ajaxDataList').html(html);
			$('#ajaxDataList').show();
			$('#ajaxDataLoading').hide();
		}
	});
}

function showInfoModal(id){
	 $.fancybox({
        'autoResize': true,
        'type': 'iframe',
        'width':750,
        'height':550,
		// 'afterClose':ajaxGetList,
        'href': '/qihooApps/appInfo/'+id        
    });
}
