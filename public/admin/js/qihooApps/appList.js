var _PageSize=15;

$(function(){
    $('#btnSubmit').click(function () {_CurrentPage=1;ajaxGetList(true); });
    $('#btnCatUpdate').click(function () {categoryUpdate(); });
    $('#btnAppUpdate').click(function () {appsUpdate(); });
    $('#btnToLewifi').click(function () {showTolewifiModal();});

    $('#sltType').change(function(){
		categoryBind();
    });
    //全选
	$("#checkAll").click(function () {
		if ($(this).is(":checked")) {
			$("input[type='checkbox']").attr("checked", "checked");
		}
		else {
			$("input[type='checkbox']").removeAttr("checked");
		}
	});
	initPage();
	categoryBind();
	// ajaxGetList(true);

})

function initPage()
{
	$.ajax({
	    type: "GET", 
	    url: "../qihooApps/summaryInfo", 
	    data: "time=" + new Date(), 
	    success: function (result) {	 
	    	$('#appsSummary').html("应用总数："+result[0]+"上次更新:"+result[1]);
	    	$('#catsSummary').html("&nbsp;&nbsp;&nbsp;&nbsp;类别总数"+result[2]+"上次更新:"+result[3]);
	    }
	});	
}

function ajaxGetList(GetPageInfo) {
	// if($("#sltCat").val()=='-1'&& $("#txtName").val().length==0)
	// {
	// 	$('#txtName').parent().after("<span class='left header-summary'>请选择子类或输入应用名称查询</span>").next('span').fadeOut(3000);
	// 	return;
	// }

	_PageSize=$('#sltPage').val();
	$('#ajaxDataList').hide();
	$('#ajaxDataLoading').show();

	$.ajax({
	    type: "GET", 
	    url: "../qihooApps/appList", 
	    data: "AjaxType=pagePageInfo&pageSize="+_PageSize+'&currentPage='+_CurrentPage
	    	+"&type="+$('#sltType').val()
	    	+"&cat="+escape($('#sltCat').val())
	    	+"&isSdk="+$('#sltIsSdk').val()
	    	+"&name="+$('#txtName').val()
	        +"&time=" + new Date(), 
	    success: function (result) {
	    	// alert(result['pageInfo']);
	    	if(GetPageInfo)
	    	{
	    		pageInfoDisplay(result['pageInfo']);
	    	}
		    // alert(result);
		     var html="";
			 if(result.pageData.length>0) 
			 {
			  	var items=result['pageData'];
			  	for(var i=0;i<items.length;i++)
			  	{
					// <td><mark><span class="highlight">无</span></mark></td>
					// alert(items[i]);
			  	 	html+='<tr>';
			    	html+='<td class="chk"><input type="checkbox" value="'+items[i]._id+'"/></td>';
			    	html+='<td>'+items[i].id+'</td>';
			  	 	html+='<td class="auto">';
			  	 	html+='<a href="#" onclick="showInfoModal(\''+items[i]._id +'\')">'+items[i].name +'</a></td>';			    	
			    	html+='<td>'+items[i].categoryName+'</td>';
			    	html+='<td>'+(items[i].incomeShare==1?'是':'否')+'</td>';			    	
			    	html+='<td>'+items[i].developer+'</td>';
			    	html+='<td>'+items[i].versionName+'</td>';
			    	html+='<td>'+items[i].downloadTimes+'</td>';
			    	html+='<td><a href="#" onclick="showAppPostModal(\''+items[i]._id +'\')">推送</a></td>';
			    	html+='</tr>';
			  }
			}else
			{
	                html="<tr><td colspan='9'>未找到任何数据</td></tr>";
			}
			$('#ajaxDataList').html(html);
			$('#ajaxDataList').show();
			$('#ajaxDataLoading').hide();
		}
	});
}

function categoryBind(){
	$.ajax({
	    type: "GET", 
	    url: "/qihooApps/getCatItems", 
	    data: "time=" + new Date(), 
	    success: function (result) {	      
	    	var html='<option value="-1">全部子类</option>';
	    	// var html='';
	    	if($("#sltType").val()!='-1'&&result[$("#sltType").val()])
	    	{
		    	for (var i = 0; i < result[$("#sltType").val()].length; i++) {
		    		html+='<option value="'+result[$("#sltType").val()][i]+'">'+result[$("#sltType").val()][i]+'</option>';
		    	};
	    	}
	    	$('#sltCat').html(html);
	    }
	});
}


function categoryUpdate()
{
	$('#btnCatUpdate').hide();
	$('#catsLoading').show();
	$.ajax({
	    type: "POST", 
	    url: "/qihooApps/catUpdate", 
	    data: {time:new Date()}, 
	    success: function (result) {	      
	    	var arr=result.split('|');
			$('#btnCatUpdate').show();
			$('#catsLoading').hide();
			categoryBind();
		    initPage();
 			
	    	alert(arr[1]);
	    }
	});
}

function appsUpdate()
{
	if($('#sltType').val()!='-1')
	{
		var type=1;
		if($("#sltType").val()=='soft')
		{	
			type=1;
		}else if($("#sltType").val()=='game')
		{
			type=2;
		}
		$('#btnAppUpdate').hide();
		$('#appsLoading').show();
		$('#appsLoading').next('span').html('更新中…').show();
		$.ajax({
		    type: "POST", 
		    url: "/qihooApps/appUpdate", 
		    timeout:12000000,
		    data:{type:type},
		    success: function (result) {	      
		    	var arr=result.split('|');
	    		$('#appsLoading').next('span').hide();
				$('#btnAppUpdate').show();
				$('#appsLoading').hide(); 		
			    initPage();	
	    		alert(arr[1]);
		    }
		});
	}else
	{
		$('#btnCatUpdate').parent().after("<span class='left header-summary'>请选择应用类型</span>").next('span').fadeOut(3000);
	}
}

function showInfoModal(id){
	 $.fancybox({
        'autoResize': true,
        'type': 'iframe',
        'width':750,
        'height':550,
		// 'afterClose':ajaxGetList,
        'href': '/qihooApps/appInfo/'+id        
    });
}

function showTolewifiModal(){
	if ($("#ajaxDataList input[type='checkbox']:checked").length > 0) {
		var _id = "";
		$("#ajaxDataList input[type='checkbox']:checked").each(function (i, c) {
			_id += $(this).val() + ",";
		});
		_id = _id.substring(0, _id.length - 1);

		$.fancybox({
	        'autoResize': true,
	        'type': 'iframe',
	        'width':500,
	        'height':400,
			// 'afterClose':ajaxGetList,
	        'href': '/qihooApps/appToLewifi/'+_id        
	    });
	}
	else {
		alert("未选择任何数据");
	}
}

function showAppPostModal(_id){
	$.fancybox({
        'autoResize': true,
        'type': 'iframe',
        'width':500,
        'height':400,
        'href': '/qihooApps/appPost/'+_id        
    });
}