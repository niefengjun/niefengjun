var _PageSize=15;

$(function(){
    $('#btnSubmit').click(function () {_CurrentPage=1;ajaxGetList(true); });
    ajaxGetList(true);
})

function ajaxGetList(GetPageInfo) {
    _PageSize=$('#sltPage').val();
    $('#ajaxDataList').hide();
    $('#ajaxDataLoading').show();
    $.ajax({
        type: "GET",
        url: "../devices/updatenotice",
        data: "AjaxType=list&pageSize="+_PageSize+'&currentPage='+_CurrentPage
            +"&time=" + new Date(),
        dataType:'JSON',
        success: function (result) {

            if(GetPageInfo)
            {
                pageInfoDisplay(result['pageInfo']);
            }

            var html="";
            if(result.pageData.length>0)
            {
                var items=result['pageData'];


                for(var i=0;i<items.length;i++)
                {
                    html+='<tr>';
                    html+='<td>'+items[i].created+'</td>';


                    html+="<td><a href=\"#\" onclick=\"return showModal(\'"+items[i]._id+"\')\" >"+items[i].title+"</a></td>";
                    html+='<td>'+items[i].starttime+'</td>';
                    html+='<td>'+items[i].endTime+'</td>';
                    html+='<td>'+items[i].create_username+'</td>';


                    html+='</tr>';

                }
            }else
            {
                html="<tr><td colspan='7'>未找到任何数据</td></tr>";
            }
            $('#ajaxDataList').html(html);
            $('#ajaxDataList').show();
            $('#ajaxDataLoading').hide();
        }
    });
}

function showModal(uuid){
    $.fancybox({
        'autoResize': true,
        'type': 'iframe',
        'width':1000,
        'height':500,
        'href': '../devices/updatenotice?AjaxType=info&id='+uuid

    });
}