var _PageSize=15;

$(function(){
    $('#btnSubmit').click(function () {_CurrentPage=1;ajaxGetList(true); });
    ajaxGetList(true);
})

function ajaxGetList(GetPageInfo) {
    _PageSize=$('#sltPage').val();
    $('#ajaxDataList').hide();
    $('#ajaxDataLoading').show();
    $.ajax({
        type: "GET",
        url: "/adminlist",
        data: "AjaxType=pagePageInfo&pageSize="+_PageSize+'&currentPage='+_CurrentPage
            +"&time=" + new Date(),
        dataType:'JSON',
        success: function (result) {

            if(GetPageInfo)
            {
                pageInfoDisplay(result['pageInfo']);
            }

            var html="";
            if(result.pageData.length>0)
            {
                var items=result['pageData'];


                for(var i=0;i<items.length;i++)
                {
                    html+='<tr>';
                    html+='<td>'+items[i].username+'</td>';


                    html+='<td>'+items[i].name+'</td>';
                    html+='<td>'+items[i].mail+'</td>';
                    html+='<td>'+items[i].phone+'</td>';
                    html+='<td><a href=/setting/managerUserEdit/?id='+items[i]._id+' >编辑</a>&nbsp;&nbsp;' ;
                    html+="<a href=#  onclick=\"return adminuserdel(\'"+items[i]._id+"\',\'"+items[i].username+"\')\" >删除</a></td>";
                    // html+='<td><a href=/setting/RoleSet/?id='+$.parseJSON(items[i])._id+'&name='+escape($.parseJSON(items[i]).rolename)+'>设置权限</td>';

                    html+='</tr>';

                }
            }else
            {
                html="<tr><td colspan='7'>未找到任何数据</td></tr>";
            }
            $('#ajaxDataList').html(html);
            $('#ajaxDataList').show();
            $('#ajaxDataLoading').hide();
        }
    });
}


function adminuserdel(uuid,u)
{
    if (confirm("确认要删除？")) {
    $.ajax({
        type: "GET",
        url: "/setting/pageadmin/",
        data: "AjaxType=adminuserDel&u=" + u + '&uuid=' + uuid + "&time=" + new Date(),
        success: function (result) {


            alert('删除成功');
            ajaxGetList(true);
        }
    });
    }
}
