
$(function(){
	$('#mod1').citySelect({
		url:'/city/',
	    prov:"",
	    city:""
	});

	$("#btnPass").click(function(){
      	passAuthInfo();
    })

    $("#btnNopass").click(function(){
    	nopassAuthInfo();
    })
})

function changeTab(index)
{
	$('#li0').removeClass('selected');
	$('#li1').removeClass('selected');
	$('#li2').removeClass('selected');
	$('#li3').removeClass('selected');
	$('#li'+index).addClass('selected');
	$('#mod0').hide();
	$('#mod1').hide();
	$('#mod2').hide();
	$('#mod3').hide();
	$('#mod'+index).show();
}

function passAuthInfo(){
	var str='';
      $("input[name=work]:checked").each(function(i){
        str+=$(this).val()+',';
      })
      str=str.substring(0,str.length-1);

      $.ajax({
        type:'post',
        url:'/channel/auth',
        data:{"authType":"pass","getprovince":$("#province").val(),"getcity":$("#city").val(),"work":str,"channelid":$("#channelid").val()},
        dataType:'json',
        success:function(data){
          if(data.status=='success'){
            //提示,关闭弹出框
            // parent.closePage($("#channelid").val());

            alert('审核成功');
          }else{
            //提示
            alert(data.msg);
          }
        }
    })
}

function nopassAuthInfo(){
	var nopass=$("#nopasstxt").val()==''?'nopass':$("#nopasstxt").val();
	$.ajax({
	    type:'post',
	    url:'/channel/auth',
	    data:{"authType":"nopass","channelid":$("#channelid").val(),"getprovince":$("#province").val(),"getcity":$("#city").val(),"txt":nopass},
	    dataType:'json',
	    success:function(data){
			if(data.status=='success'){
			//提示,关闭弹出框
            alert('审核成功');
			}else{
			//提示
			alert(data.msg);
			}
	    }
	})
}