
$(function(){
	$('#mod1').citySelect({
		url:'/city/',
	    prov:$('#hidConferArea').val().split(',')[0]?$('#hidConferArea').val().split(',')[0]:"",
	    city:$('#hidConferArea').val().split(',')[1]?$('#hidConferArea').val().split(',')[1]:"",
	    provPre:{text:"请选择",value:"-1"},
	    cityPre:{text:"请选择",value:"-1"}
	});
	$('#area').citySelect({
		url:'/city/',
	    prov:$('#hidArea').val().split(',')[0]?$('#hidArea').val().split(',')[0]:"",
	    city:$('#hidArea').val().split(',')[1]?$('#hidArea').val().split(',')[1]:"",
	    provPre:{text:"请选择",value:"-1"},
	    cityPre:{text:"请选择",value:"-1"}
	});

	$("#btnSubmit").click(function(){
      	passAuthInfo();
    })

    pageInit();
})

function pageInit(){

	if($('#hidStatus').val()=='verified'){
		$('#btnSubmit').show();
	}
	if($('#hidProfession').val().length>0)
	{
		var listProfession=$('#hidProfession').val().split(',');
		for (var i = 0; i < listProfession.length; i++) {
			$("input[name=work]").each(function(j){
				if(listProfession[i]==$(this).val())
				{
					$(this).attr('checked','checked');
				}
			})
		};
	}
}

function changeTab(index)
{
	$('#li0').removeClass('selected');
	$('#li1').removeClass('selected');
	$('#li2').removeClass('selected');
	$('#li'+index).addClass('selected');
	$('#mod0').hide();
	$('#mod1').hide();
	$('#mod2').hide();
	$('#mod'+index).show();
}

function passAuthInfo(){
	var str='';
      $("input[name=work]:checked").each(function(i){
        str+=$(this).val()+',';
      })
      str=str.substring(0,str.length-1);

      $.ajax({
        type:'post',
        url:'/channel/info',
        data:{"getprovince":$("#province").val(),"getcity":$("#city").val(),"work":str,"channelid":$("#hidChannelid").val(),"areaProvince":$("#areaProvince").val(),"areaCity":$("#areaCity").val()},
        dataType:'json',
        success:function(data){
          if(data.status=='success'){
            alert('更新成功');
          }else{
            //提示
            alert(data.msg);
          }
        }
    })
}
