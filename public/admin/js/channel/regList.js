var _PageSize=15;

$(function(){
	$('#area').citySelect({
		url:'/city/',
	    prov:"",
	    city:"",
	    provPre:{text:"全部省份",value:"-1"},
	    cityPre:{text:"全部城市",value:"-1"}
	});
    $('#btnSubmit').click(function () {_CurrentPage=1;ajaxGetList(true); });
	ajaxGetList(true);
})

function ajaxGetList(GetPageInfo) {
	_PageSize=$('#sltPage').val();
	$('#ajaxDataList').hide();
	$('#ajaxDataLoading').show();

	$.ajax({
	    type: "GET", 
	    url: "../channel/regList", 
	    data: "AjaxType=pagePageInfo&pageSize="+_PageSize+'&currentPage='+_CurrentPage
	    	+"&email="+$('#txtEmail').val()
	    	+"&name="+escape($('#txtName').val())
	    	+"&channelId="+$('#txtID').val()
	    	+"&status="+$('#sltStatus').val()
	    	+"&province="+($('#sltProvince').val()==null?"-1":$('#sltProvince').val())
	    	+"&city="+($('#sltCity').val()==null?"-1":$('#sltCity').val())
	        +"&time=" + new Date(), 
	    success: function (result) {
	    	// alert(result['pageInfo']);
	    	if(GetPageInfo)
	    	{
	    		pageInfoDisplay(result['pageInfo']);
	    	}
		    // alert(result);
		     var html="";
			 if(result.pageData.length>0) 
			 {
			  	var items=result['pageData'];
			  	for(var i=0;i<items.length;i++)
			  	{
					// <td><mark><span class="highlight">无</span></mark></td>
					// alert(items[i]);
			  	 	html+='<tr>';
			    	html+='<td>'+items[i].channelID+'</td>';
			  	 	html+='<td class="auto">';
			  	 	html+='<a href="#" onclick="showModal(\''+items[i].user +'\',600)">'+items[i].nickName +'</a></td>';			    	
			    	html+='<td>'+items[i].email+'</td>';
			    	html+='<td>'+(items[i].company?items[i].company:"")+'</td>';			    	
			    	html+='<td>'+(items[i].area?items[i].area:"")+'</td>';
			    	html+='<td>'+(items[i].mobile?items[i].mobile:"")+'</td>';
			    	if(items[i].status=='verify')
			    	{
			    		html+='<td>未提交</td>';
			    	}else if(items[i].status=='verified')
			    	{
			    		html+='<td>已通过</td>';
			    	}else if(items[i].status=='noPass')
			    	{
			    		html+='<td>未通过</td>';
			    	}else  if(items[i].status=='waitVerify')
			    	{
			    		html+='<td>待审核</td>';
			    	}else  if(items[i].status=='blocked')
			    	{
			    		html+='<td>锁定</td>';
			    	}
			    	html+='<td><a href="#" onclick="showModal(\''+items[i].user +'\')">查看</a></td>';
			    	html+='</tr>';

			  }
			}else
			{
	                html="<tr><td colspan='8'>未找到任何数据</td></tr>";
			}
			$('#ajaxDataList').html(html);
			$('#ajaxDataList').show();
			$('#ajaxDataLoading').hide();
		}
	});
}


function showModal(uuid){
	 $.fancybox({
        'autoResize': true,
        'type': 'iframe',
        'width':750,
        'height':550,
		'afterClose':ajaxGetList,
        'href': '/channel/info/'+uuid        
    });
}