/**
 * Created by zhaomenmengxi on 14-1-26.
 */


function getlist(){
    $.ajax({
        url: '/app/getlist',
        type: 'get',
        data: 'type=list',
        success: function (data) {
            console.log(data)
            var list
            for (i = 0; i in data; i++) {
                list = list + '<tr><td>'+data[i].appName+'</td><td>'+data[i].packageName+'</td><td>'+data[i].versionCode+'</td><td>'+data[i].versionName+'</td><td>'+data[i].appUrl+'</td><td>'+data[i].os+'</td><td>'+data[i].created+'</td><td>'+data[i].mark+'</td></tr>'
            }
            ;
            $('#ajaxDataList').html(list);
        }
    })
}

$(function () {
  getlist();
    $('#addversion').click(function(){
        $('.modal').show();
    })
    $('#closeme').click(function(){
        $('.modal').hide();
    })
    $('#add').click(function(){
        var appName=$('#appname').val(),
            packageName=$('#packagename').val(),
            versionCode=$('#versioncode').val(),
            versionName=$('#versionname').val(),
            appUrl=$('#url').val(),
            os=$('#os').val(),
            mark=$('#mark').val();
        $.ajax({
            url:'/app/getlist',
            type:'get',
            data:'type=add&appName='+appName+'&packageName='+packageName+'&versionCode='+versionCode+'&versionName='+versionName+'&appUrl='+appUrl+'&os='+os+'&mark='+mark,
            success:function(data){
                if(data=='1'){
                    $('.modal').hide();
                    getlist();
                }
            }
        })
    })
})