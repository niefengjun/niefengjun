var db=require('./db').getCollection('dining') ;
function add(ret,callback){

    db.insert(ret,function(e,v){
        callback(e,v) ;
     }) ;
}

function info(user,dining_name,callback) {

    db.findOne({create_username:user,dining_name:dining_name}, function (key, value) {
            callback(key, value);
        });
}

function list(user,callback){

    db.find({create_username:user}).toArray(function (key, list) {
        callback(key, list);
    });
}

function listinfo(id,callback) {

    db.findOne({_id:id}, function (key, value) {

            callback(key, value);
        });
}

exports.add = add;
exports.info = info;
exports.list = list;
exports.listinfo=listinfo;