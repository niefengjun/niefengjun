
var db=require('./db').getCollection('mp_User') ;
function add(ret,callback)
{
    db.insert(ret,function(e,v)
        {
            callback(e,v) ;
        }
    ) ;
}

function list(callback)
{
    db.find().sort({created:-1}).toArray(function (key, list) {

        callback(key, list);
    });
}

function hotlist(callback)
{
    db.find().sort({yuedu:-1}).toArray(function (key, list) {

        callback(key, list);
    });
}

function blogDel(uuid, callback) {

    db.remove({_id:uuid}, function (key, value) {

            callback(key, value);
        }
    );

}


function info(id,callback) {

    db.findOne({_id:id}, function (key, value) {

            callback(key, value);
        }
    );

}

function usernameinfo(id,callback) {

    db.findOne({username:id}, function (key, value) {

            callback(key, value);
        }
    );

}
function blogedit(uuid, info, callback) {


    db.findAndModify({_id: uuid}, [], info, function (key, value) {
        callback(null,'修改成功');
    });

}
function findOneid(id1,callback)
{

    db.find({_id:id1}).toArray(function (key, v) {
        if(v[0].yuedu)
        {
            v[0].yuedu=v[0].yuedu+1 ;
        }
        else
        {
            v[0].yuedu=1 ;
        }
        db.update({_id:id1},v[0], function (e, v1) {

            callback(key, v);
        } )
    });


}
exports.add=add ;
exports.list=list ;
exports.findOneid=findOneid ;
exports.blogDel=blogDel ;
exports.info=info ;
exports.blogedit=blogedit ;
exports.hotlist=hotlist ;
exports.usernameinfo=usernameinfo ;