var db1=require('./db').getCollection('blog') ;
var db = require('./db').getnedb('blog');
function add(ret, callback) {
    db.insert(ret, function (e, v) {
            callback(e, v);
        }
    );
}

//function updateshuju(callback)
//{
//    db1.find().sort({created:-1}).toArray(function (key, list) {
//         add(list,function(e,v)
//             {
//                 callback(e,v);
//             }
//         )
//
//    });
//}
function list(callback) {
    //db.find().sort({created:-1}).toArray(function (key, list) {
    //
    //    callback(key, list);
    //});

    db.find({}).sort({created: -1}).skip().exec(function (key, list) {
        // docs is [doc3, doc1]
        callback(key, list);
    });
}

function categorylist(category1,callback) {

    //db.find().sort({created:-1}).toArray(function (key, list) {
    //
    //    callback(key, list);
    //});

    db.find({column:category1}).sort({created: -1}).skip().exec(function (key, list) {
        // docs is [doc3, doc1]
        callback(key, list);
    });
}


function hotlist(callback) {
    //db.find().sort({yuedu:-1}).toArray(function (key, list) {
    //    callback(key, list);
    //});

    db.find({}).sort({yuedu: -1}).skip().exec(function (key, list) {
        // docs is [doc3, doc1]
        callback(key, list);
    });
}

function blogDel(uuid, callback) {

    db.remove({_id: uuid}, function (key, value) {

            callback(key, value);
        }
    );

}


function info(id, callback) {

    db.findOne({_id: id}, function (key, value) {

            callback(key, value);
        }
    );

}

function blogedit(uuid, info, callback) {


    db.update({_id: uuid}, info, function (key, value) {
        callback(null, '修改成功');
    });

}
function findOneid(id1, callback) {

    db.findOne({_id: id1}, function (key, v) {

        if (v.yuedu) {
            v.yuedu = v.yuedu + 1;
        }
        else {
            v.yuedu = 1;
        }

        db.update({_id: id1}, v, function (e, v1) {

            callback(key, v);
        })
    });


}
exports.add = add;
exports.list = list;
exports.findOneid = findOneid;
exports.blogDel = blogDel;
exports.info = info;
exports.blogedit = blogedit;
exports.hotlist = hotlist;
exports.categorylist=categorylist ;
//exports.updateshuju=updateshuju ;