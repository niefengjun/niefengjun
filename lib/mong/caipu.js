var db = require('./db').getnedb('api.caipu');
var caipumenu = require('./db').getnedb('api.caipumenu');
function addtype(ret, callback) {
    db.insert(ret, function (e, v) {
            callback(e, v);
        }
    );
}

function list(callback) {
    caipumenu.find().sort({_id: -1}).skip().exec(function (key, list) {

        callback(key, list);
    });
}

function findOne(username, callback) {
    caipumenu.find({title: /username/}).skip().exec(function (key, list) {

        callback(key, list);
    });
}
function addmenu(ret, callback) {
    caipumenu.insert(ret, function (e, v) {
            callback(e, v);
        }
    );
}
exports.addmenu = addmenu;
exports.addtype = addtype;
exports.list = list;
exports.findOne = findOne;
