var isDevelopment = process.env.NODE_ENV == "development" ? true : false;
exports.config = {

    niefengjun: 'mongodb://127.0.0.1:27017/niefengjun',
    caipukey: '5e7653eceeaa75aff3306a9af15f59ef',
    jiqiren: "ee609f95ca248d12aa42475c167653f8",
    secret: '5e7653eceeaa75aff3306a9af15f59ef',
    title: "聂峰军个人网站",
    description: "关注it前沿",
    //栏目设置
    menu: [
        {m: 'nodejs', url: '/category/nodejs.html', name: 'nodejs'}
        , {
            m: 'life',
            url: '/category/life.html',
            name: '生活'
        }
        , {m: 'reprint', url: '/category/reprint.html', name: '转载'}
        , {
            m: 'Other',
            url: '/category/Other.html',
            name: '其他'
        }
        , {
            m: 'ip地址',
            url: '/ip',
            name: 'ip地址'
        }
    ]
}