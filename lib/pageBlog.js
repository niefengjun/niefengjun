var blog = require('./mong/blog');
var tool = require('./tool');
exports.index = function (req, res) {

    var AjaxType = req.param('ajax');
    // console.log(AjaxType);
    switch (AjaxType) {
        case "add":
            blogadd(req, res);
            break;
        case "newsedit":
            newsedit(req, res);
            break;
        case "blogDel":
            blogDel(req, res);
            break;
        case "blogedit":
            blogedit(req, res);
            break;
        case "bloginfo":
            bloginfo(req, res);
            break;
        case "pagePageInfo":
            pagePageInfo(req, res);
            break;
        case "edit":
            var id = req.query.id;
            res.render("admin/blogedit.html", {id: id});
            break;
        case "list":
            res.render("admin/bloglist");
            break;
        default :
            res.render("admin/blogIndex");
            break;
    }
};


function blogedit(req, res) {
    var id = req.param('id');



    var content = unescape(req.param('c'));
    var t = unescape(req.param('t'));
    var column=req.param('column') ;

    var myDate = new Date();
    var time1 = tool.formatFullDate(myDate);
    blog.info(id, function (k, v) {

            var info = v;
            info.title = t;
            info.column=column ;

            info.content = content;

            info.updated = time1;
            info.update_username = tool.cookuserinfo(req).username;

            blog.blogedit(id, info, function (k, v) {
                    res.send(v);
                }
            );
        }
    );
    // var info ={title:''+title+'',content:''+content+'',created:''+time+'',create_username:''+req.cookies["wifiadminuser"].toString() +'',updated:'',update_username:'',type:''};

}
function bloginfo(req, res) {
    var id = req.query.id;
    console.log(id);
    blog.info(id, function (k, v) {
            res.send(v);
        }
    );
}
function blogDel(req, res) {
    var id = req.query.id;


    blog.blogDel(id, function (k, v) {
            res.send('1');
        }
    );
}
function pagePageInfo(req, res) {
    var cSize = req.query.pageSize;
    var cPage = req.query.currentPage;

    blog.list(function (err, list) {
       
        list.sort(tool.sortJSONArry('created', true, Date.parse));
        var ret = {
            pageInfo: tool.getPageInfo(list.length, cSize),
            pageData: tool.getPageData(list, cSize, cPage)
        }

        res.send(ret);
    })
}

//博客添加
function blogadd(req, res) {
    var title = (unescape(req.param('t')));
    var content = (unescape(req.param('c')));
    var myDate = new Date();
    var time = tool.formatFullDate(myDate);
    var column=req.param('column') ;
    var info = {
        _id: tool.generateUUID(32),
        title: title,
        content: content,
        column:column,
        yuedu: 0,
        created: time,
        create_username: tool.cookuserinfo(req).username
    };
    blog.add(info, function (k, v) {
            if (!k) {
                res.send('1');
            }
            else {
                res.send('0')
            }
        }
    );
}