var dingcanUser = require('./mong/dingcanUser');
var dingcan1 = require('./mong/dingcan');
var tool = require('./tool');
function index(req, res) {

    var AjaxType = req.query.ajax;
    var userid = req.query.userid;
    switch (AjaxType) {
        case "reg":

            res.render('dingcan/reg.html', {userid: userid});
            break;
        case "dingcan":

            res.render('dingcan/dingcan.html', {userid: userid});
            break;
        case "reguser":
            reguser(req, res);
            break;
        case "dingcanadd":
            dingcan(req, res);
            break;
        case "findcreatetime":
            findcreatetime(req, res);
            break;

        case "denglu":
            dingcanUser.findopenid(userid, function (e, v) {
                if (!e && v && v.length > 0) //已经登记
                {
                    var ret = '/dingcan/?ajax=dingcan&userid=' + userid;

                    res.redirect(ret);
                }
                else {
                    var ret = '/dingcan/?ajax=reg&userid=' + userid;

                    res.redirect(ret);
                }
            });
            break;
        case "pagePageInfo":
            pagePageInfo(req, res);
            break;
        default :
            res.render('dingcan/index.html', {userid: userid});

            break;
    }
}

function dingcan(req, res) {
    var info = tool.parseJSON(req.query.info);
    var myDate = new Date();
    dingcanUser.findopenid(info.openid, function (e1, v1) {
        info.username=v1[0].username ;
        info.shouji=v1[0].shouji ;
        info.createtime=tool.getYYYYMMDD(myDate);
        info.time=tool.formatFullDate(myDate) ;
        dingcan1.dingcan(info, function (e, v) {
                res.send(e);
            }
        )
    })
}

function findcreatetime(req,res)
{
    var myDate = new Date();
    var createtime=tool.getYYYYMMDD(myDate);
    dingcan1.findcreatetime(createtime,function(e,v)
        {
            res.send(v);
        }
    )
}

function reguser(req, res) {
    var info = tool.parseJSON(req.query.info);

    dingcanUser.denglu(info, function (e, v) {
            res.send('1');
        }
    )
}

exports.index = index;
