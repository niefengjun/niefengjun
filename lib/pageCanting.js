var ct = require('./mong/canting');
var tool = require('./tool');
exports.index = function (req, res) {

    var AjaxType = req.param('AjaxType');

    switch (AjaxType) {
        case "add":
            pageadd(req, res);
            break;
        case "list":
            pagelist(req, res);
            break;
        case "listOneName":
            listOneName(req,res);
            break;
        default :
            var user = tool.cookCt(req).username;
            res.render('restaurant.html',{username:user});
            break ;

    }
};

function pageadd(req,res){
    var dining_name = (unescape(req.param('dining_name')));
    var username = (unescape(req.param('username')));
    var myDate = new Date();
    var time = tool.formatFullDate(myDate);
    var info = {
        _id: tool.generateUUID(32),
        dining_name : dining_name,
        created: time,
        create_username: username
    };
    ct.info(username,dining_name,function(err,value){
        if (!err&&!value) {
            ct.add(info, function (k, v) {
                if(!k){
                    res.send('1');
                }
            });

        }else {
            res.send('-1');
        }

    });
}

function pagelist(req,res){
    var username = (unescape(req.param('username')));
    ct.list(username,function(err,result){
        if(!err){
            res.send(result);
        }
    });
}

function listOneName(req,res){
    var id = req.param('id');

    ct.listinfo(id,function(err,result){
        console.log("listone:  "+err,result);
        if(!err){
            res.send(result);
        }else{
            res.send('0');
        }
    })
}
