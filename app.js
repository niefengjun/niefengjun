/**
 * Module dependencies.
 */

var express = require('express');
var http = require('http');
var path = require('path');
var ejs = require('ejs');
var app = express();

var ueditor = require("ueditor");
var routes = require('./routes/appRoutes');
var mp_routes = require('./routes/mp_routes');
var job_routes = require('./routes/job_routes');
var bodyParser = require('body-parser');
var log4js = require('log4js');
log4js.configure('config/log4js.json');
global.logger = log4js.getLogger();
app.use(log4js.connectLogger(logger,
    {
        format: ':method :url HTTP/:http-version :status :res[content-length] - :response-time ms',
        nolog: ["\\.jpg","\\.ico", "\\.png", "\\.gif", "\\.js", "\\.css", "\\.swf"]
    }));


// all environments
app.set('port', process.env.PORT || 4000);

//app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.cookieParser('niefengjun8644r4vdfdf'));
app.use(express.session());
app.use(express.methodOverride());

app.use(app.router);
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(bodyParser.json());
//app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, 'public')));
app.set('views', path.join(__dirname, 'views'));
app.engine('.html', ejs.__express);
app.set('view engine', 'html');
mp_routes(app);//公众号路由
job_routes(app);//微招聘
routes(app);
app.use("/ueditor/ue/", ueditor(path.join(__dirname, 'public'), function (req, res, next) {
    // ueditor 客户发起上传图片请求

    if (req.query.action === 'uploadimage') {
        var foo = req.ueditor;
        var date = new Date();
        var imgname = req.ueditor.filename;

        var img_url = '/images/ueditor/';
        res.ue_up(img_url); //你只要输入要保存的地址 。保存操作交给ueditor来做
    }
    //  客户端发起图片列表请求
    else if (req.query.action === 'listimage') {
        var dir_url = '/images/ueditor/';
        res.ue_list(dir_url); // 客户端会列出 dir_url 目录下的所有图片
    }
    // 客户端发起其它请求
    else {
        // console.log('config.json')
        res.setHeader('Content-Type', 'application/json');
        res.redirect('/ueditor/nodejs/config.json');
    }
}));
app.use(app.router);
app.use(function (req, res, next) {
    res.redirect('/');
});
app.use(express.bodyParser({keepExtensions: true, uploadDir: '/tmp'}));
app.listen(app.get('port'),"127.0.0.1");
console.log('Express server listening on port ' + app.get('port'));
